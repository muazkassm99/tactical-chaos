package sample.Model.Itmes;

import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;

public class NightShift extends Item {

    NightShift() {

    }

    public NightShift(int id)
    {
        super(ItemsClassNames.NIGHTSHIFT.name(), id);
        this.setPhysicalDamageIncrease(20);

        this.setChampionClass(ChampionClass.ASSASSIN);
    }


    @Override
    public String toString() {
        return "Night Shift";
    }
}
