package sample.Controller;

import sample.Model.Champion;
import sample.Model.Itmes.Item;
import sample.View.Player;

import java.nio.DoubleBuffer;

public class BuffManager {

    public BuffManager() {
    }

    /**
     * applies buff into the player champions when called
     * (buffs are applied to all the champions of the player).
     *
     * @param player
     * @return
     */
    public Player applyItemBuffs(Player player)
    {

        for (Champion ch: player.getBattleArenaManager().getBattleArenaChampions(player.getId())) {
            for (Item item: ch.getItems()) {

                ch = doBuffItem(ch, item);
            }
        }

        return player;
    }


    /**
     * this method applies debuff to the player champions
     * (deBuffs are made to all the champions of the player).
     * @param player
     * @return
     */
    public Player applyItemsDebuff(Player player)
    {
        for (Champion ch: player.getBattleArenaManager().getBattleArenaChampions(player.getId())) {
            for (Item item: ch.getItems()) {

                ch = doDeBuffItem(ch, item);
            }
        }
        return player;
    }


    /**
     * used to set all the attributes of the item to the champion and buff him
     * working with the equation : (((new = old + old*increaseValue))));
     * @param ch
     * @param item
     * @return
     */
    public Champion doDeBuffItem(Champion ch, Item item) {

        //ch.setAbilityDmg
        /*ch.setBasicAttackDamage((int) item.getPhysicalDamageIncrease().calculateFinalValue(ch.getBasicAttackDamage()));
        ch.setArmor((int) item.getArmorIncrease().calculateFinalValue(ch.getArmor()));
        ch.setCriticalStrikeChance((int) item.getCriticalStrikeIncrease().calculateFinalValue(ch.getCriticalStrikeChance()));
        ch.setHealth((int) item.getMaxHealthIncrease().calculateFinalValue(ch.getHealth()));
        ch.setMagicResist((int) item.getMagicResistIncrease().calculateFinalValue(ch.getMagicResist()));
*/

        if(item == null)
            return ch;

        ch.setBasicAttackDamage((int) (((ch.getBasicAttackDamage()*1.0)/(1+item.getPhysicalDamageIncrease()/100.0))));
        ch.setArmor((int) (((ch.getArmor()*1.0)/(1+item.getArmorIncrease()/100.0))));
        ch.setCriticalStrikeChance((int) (((ch.getCriticalStrikeChance()*1.0)/(1+item.getCriticalStrikeIncrease()/100.0))));
        ch.setHealth((int) (((ch.getHealth()*1.0)/(1+item.getMaxHealthIncrease()/100.0))));
        ch.setMagicResist((int) (((ch.getMagicResist()*1.0)/(1+item.getMagicResistIncrease()/100.0))));



        return ch;
    }


    /**
     * used to set all the attributes of the item to the champion and buff him
     * working with the equation : old = new / (increasePercentage + 1);
     * @param ch
     * @param item
     * @return
     */
    public Champion doBuffItem(Champion ch, Item item) {

        if(item == null)
            return ch;
        ch.setBasicAttackDamage((ch.getBasicAttackDamage() + (ch.getBasicAttackDamage()*item.getPhysicalDamageIncrease()/100)));
        ch.setArmor((ch.getArmor() + (ch.getArmor()*item.getArmorIncrease()/100)));
        ch.setHealth((ch.getHealth() + (ch.getHealth()*item.getMaxHealthIncrease()/100)));
        ch.setCriticalStrikeChance((ch.getCriticalStrikeChance() + (ch.getCriticalStrikeChance()*item.getCriticalStrikeIncrease()/100)));
        ch.setMagicResist((ch.getMagicResist() + (ch.getMagicResist()*item.getMagicResistIncrease()/100)));

        return ch;
    }

}
