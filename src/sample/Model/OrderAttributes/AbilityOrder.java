package sample.Model.OrderAttributes;

public class AbilityOrder extends Order {

    private int attackedChampionId;

    AbilityOrder() {
        super(OrderClassNames.ABILITYORDER.name());
    }

    public AbilityOrder(int championId, int attackedChampionId) {
        this();
        this.championId = championId;
        this.attackedChampionId = attackedChampionId;
    }

    public int getAttackedChampionId() {
        return attackedChampionId;
    }

    public void setAttackedChampionId(int attackedChampionId) {
        this.attackedChampionId = attackedChampionId;
    }
}
