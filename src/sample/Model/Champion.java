package sample.Model;

import sample.Model.Enums.ChampionClass;
import sample.Model.Enums.ChampionPlace;
import sample.Model.Enums.ChampionStatus;
import sample.Model.Itmes.Item;

import java.util.ArrayList;
import java.util.List;

public class Champion {
    private List <Item> items;
    private int uniqueID;
    private String name;
    private int playerId;
    private List<ChampionClass> classes;
    private ChampionStatus status;
    private int level;
    private int health;
    private int currentHealth;
    private int armor;
    private int manaCost;
    private int currentMana;
    private int magicResist;
    private int visionRange;
    private int attackRange;
    private int movementSpeed;
    private int basicAttackDamage;
    private int criticalStrikeDamage;
    private int criticalStrikeChance;
    private int price;
    private int stunnedFor;
    private ChampionPlace place;
    private Ability ability;

    public Champion() {
    }

    public Champion(String name) {
        this.setName(name);
    }

    public Champion(int uniqueID, String name,
                    int level,
                    int health,
                    List<ChampionClass> classes,
                    int armor,
                    int manaCost,
                    int currentMana,
                    int magicResist,
                    int visionRange,
                    int attackRange,
                    int movementSpeed,
                    int basicAttackDamage,
                    int criticalStrikeDamage,
                    int criticalStrikeChance,
                    int price) {
        this.items = new ArrayList<>();
        this.uniqueID = uniqueID;
        this.name = name;
        this.classes = classes;
        this.playerId = -1;
        this.status = ChampionStatus.ALIVE;
        this.level = level;
        this.health = health;
        this.currentHealth = health;
        this.armor = armor;
        this.manaCost = manaCost;
        this.currentMana = currentMana;
        this.magicResist = magicResist;
        this.visionRange = visionRange;
        this.attackRange = attackRange;
        this.movementSpeed = movementSpeed;
        this.basicAttackDamage = basicAttackDamage;
        this.criticalStrikeDamage = criticalStrikeDamage;
        this.criticalStrikeChance = criticalStrikeChance;
        this.price = price;
        this.place = ChampionPlace.STORE;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public List<ChampionClass> getClasses() {
        return classes;
    }

    public void setClasses(List<ChampionClass> classes) {
        this.classes = classes;
    }

    public ChampionStatus getStatus() {
        return status;
    }

    public void setStatus(ChampionStatus status) {
        this.status = status;
    }

    public int getHealth() {
        return health;
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }

    public int getArmor() {
        return armor;
    }

    public int getManaCost() {
        return manaCost;
    }

    public int getCurrentMana() {
        return currentMana;
    }

    public void setCurrentMana(int currentMana) {
        this.currentMana = currentMana;
    }

    public int isMagicResist() {
        return magicResist;
    }

    public int getVisionRange() {
        return visionRange;
    }

    public int getAttackRange() {
        return attackRange;
    }

    public int getMovementSpeed() {
        return movementSpeed;
    }

    public int getBasicAttackDamage() {
        return basicAttackDamage;
    }

    public int getCriticalStrikeDamage() {
        return criticalStrikeDamage;
    }

    public int getCriticalStrikeChance() {
        return criticalStrikeChance;
    }

    public int getPrice() {
        return price;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public void setManaCost(int manaCost) {
        this.manaCost = manaCost;
    }

    public int getMagicResist() {
        return magicResist;
    }

    public void setMagicResist(int magicResist) {
        this.magicResist = magicResist;
    }

    public void setVisionRange(int visionRange) {
        this.visionRange = visionRange;
    }

    public void setAttackRange(int attackRange) {
        this.attackRange = attackRange;
    }

    public void setMovementSpeed(int movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public void setBasicAttackDamage(int basicAttackDamage) {
        this.basicAttackDamage = basicAttackDamage;
    }

    public void setCriticalStrikeDamage(int criticalStrikeDamage) {
        this.criticalStrikeDamage = criticalStrikeDamage;
    }

    public void setCriticalStrikeChance(int criticalStrikeChance) {
        this.criticalStrikeChance = criticalStrikeChance;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getStunnedFor() {
        return stunnedFor;
    }

    public void setStunnedFor(int stunnedFor) {
        this.stunnedFor = stunnedFor;
    }

    public ChampionPlace getPlace() {
        return place;
    }

    public void setPlace(ChampionPlace place) {
        this.place = place;
    }

    public Ability getAbility() {
        return ability;
    }

    public void setAbility(Ability ability) {
        this.ability = ability;
    }

    @Override
    public boolean equals(Object obj) {
        Champion passedChamp = (Champion) obj;
        return passedChamp.getUniqueID() == this.getUniqueID();
    }

    @Override
    public String toString() {
//        return (uniqueID + " " + name + " " + price + " " + currentHealth + " " + armor + " " +
//                magicResist + " " + visionRange + " " + attackRange + " " + basicAttackDamage + " " +
//                movementSpeed + " " + criticalStrikeChance + " " + criticalStrikeDamage + " " + currentMana + " " + manaCost );

        return (uniqueID + " " + name + " $: " + price + " Dmg: " + basicAttackDamage + " ARM: " + armor + " CSC: "
                + criticalStrikeChance + " HP: " + health + " MR: " + magicResist);
    }

    public int getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(int uniqueID) {
        this.uniqueID = uniqueID;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isStunned() {
        if (this.stunnedFor != 0) {
            return true;
        }
        return false;
    }


    public List<Item> getItems() {
        return items;
    }

    public boolean addItem(Item item)
    {
        return this.items.add(item);
    }

    public boolean removeItem(Item item)
    {
        for (Item it:this.items) {
            if(item.getUniqueID() == it.getUniqueID())
                return this.items.remove(item);
        }
        return false;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
