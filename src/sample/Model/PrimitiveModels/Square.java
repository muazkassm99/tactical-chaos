package sample.Model.PrimitiveModels;

import sample.Model.Champion;
import sample.Model.Enums.TileType;
import sample.Model.Itmes.Item;

import java.util.ArrayList;
import java.util.List;

public class Square {
    private List<Champion> champions;

    private Item itemOnSquare;

    private TileType type;

    public Square() {
        champions = new ArrayList<>();
        itemOnSquare = null;
        type = null;
    }

    public List<Champion> getChampions() {
        return champions;
    }

    public void setChampions(List<Champion> champions) {
        this.champions = champions;
    }

    public Item getItemOnSquare() {
        return itemOnSquare;
    }

    public void setItemOnSquare(Item itemOnSquare) {
        this.itemOnSquare = itemOnSquare;
    }

    public TileType getType() {
        return type;
    }

    public void setType(TileType type) {
        this.type = type;
    }
}
