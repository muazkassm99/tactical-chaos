package sample.View;

import javafx.util.Pair;
import sample.ColorsPrinting;
import sample.Controller.Exceptions.InTerrainException;
import sample.Controller.Exceptions.OutOfArenaException;
import sample.Controller.SaveGameCallBack;
import sample.Controller.StoreManager;
import sample.Model.Champion;
import sample.Model.Enums.Direction;
import sample.Model.Enums.TileType;
import sample.Model.Itmes.Item;
import sample.Model.OrderAttributes.AbilityOrder;
import sample.Model.OrderAttributes.Attack;
import sample.Model.OrderAttributes.Order;
import sample.Model.OrderAttributes.Walk;
import sample.Model.PrimitiveModels.Square;
import sample.Utilities.Globals;

import java.util.List;

public class ConsolePlayer extends Player {

    ConsolePlayer() {

    }

    public ConsolePlayer(int id, SaveGameCallBack saveGameCallBack) {
        super(PlayerType.CONSOLE.name(), id, saveGameCallBack);
    }

    public ConsolePlayer(int id, SaveGameCallBack saveGameCallBack, StoreManager storeManager) {
        super(PlayerType.CONSOLE.name(), id, saveGameCallBack, storeManager);
    }

    @Override
    public void startPlanningPhase(boolean beforeNine) {

        // number of swaps performed in this round
        int numberOfSwaps = 0;
        int numberOfPurchases = 0;

        while (true) {

            System.out.println("**************************************************");
            System.out.println("Player #" + id + " turn.");
            if(!beforeNine) {
                System.out.println("Coins: " + coins);
            }

            System.out.println("Round : " + Globals.getCurrentRound());

            System.out.println("Enter your choice:\n" +
                    "'buy' for purchasing champions from store.\n" +
                    "'sell' for selling champions from store.\n" +
                    "'swap' for swapping champions between arena and bench.\n" +
                    ((!beforeNine) ? "'order' for giving orders to arena champions.\n" : "") +
                    "'arena' for displaying the arena.\n" +
                    "'bench' for displaying the bench.\n" +
                    "'pia' for placing a champion from bench to arena.\n" +
                    "'pib' for placing a champion from arena to bench.\n" +
                    "'square' for displaying a square's data.\n"+
                    "'save' for for saving the current game.\n" +
                    "'done' for ending your planning phase.");

            String input = Globals.getScanner().next();
            if (input.equalsIgnoreCase("done")) {
                System.out.println("Player's #" + id + " has ended.");
                break;
            } else if (input.equalsIgnoreCase("buy")) {
                purchaseProcess();
                for(int  i = 0; i < 3; i++) {
                    upgradeChampions();
                }
            } else if (input.equalsIgnoreCase("sell")) {
                sellProcess();
            } else if (input.equalsIgnoreCase("swap")) {
                if (numberOfSwaps != Globals.getMaxNumberOfSwaps()) {
                    swapProcess();
                    numberOfSwaps++;
                }
            } else if (input.equalsIgnoreCase("order")) {
                if (!beforeNine) {
                    Order order = collectOrder();
                    if (order != null) {
                        orders.add(order);
                    } else {
                        System.out.println("Order canceled");
                    }
                }
            } else if(input.equalsIgnoreCase("arena")) {
                displayArena();
            } else if(input.equalsIgnoreCase("bench")) {
                displayBench();
            } else if(input.equalsIgnoreCase("pia")) {
                placeInArenaProcess();
            } else if(input.equalsIgnoreCase("pib")) {
                placeInBenchProcess();
            } else if(input.equalsIgnoreCase("save")) {
                saveGame();
                System.out.println("Game saved.");
            }
//            else if(input.equalsIgnoreCase("items")){
//
//                System.out.println("number of items on Arena : " + battleArenaManager.collectItemsFromArena().size() );
//            }
            else if(input.equalsIgnoreCase("square")){
                displaySquare();
            }
            else {
                System.out.println("No such choice, re-enter your choice.");
            }
        }

    }

    private void displaySquare() {
        System.out.println("Enter i and j for the square you want to display data to :");
        int i = Globals.getScanner().nextInt(), j = Globals.getScanner().nextInt();

        try{
            i--;
            j--;
            Item item = battleArenaManager.getItemInASquare(i, j);
            List<Champion> champions= battleArenaManager.getArena().getSquares()[i][j].getChampions();
            if(item != null)
            {
                System.out.println("Item : " + item);
            }
            if(champions.size()!=0)
            {
                System.out.println("Champions : ");
                for (Champion champion:champions) {
                    int playerId = champion.getPlayerId();
                    String owner;
                    if(playerId == this.getId())
                        owner = "ME";
                    else
                        owner = String.valueOf(playerId);
                    System.out.println(champion.getName() + " champion of player : " + owner);
                }
            }
            else {
                if(item == null)
                {
                    System.out.println("Square is empty!");
                }
            }


        }catch (Exception ee){
            System.out.println("invalid i and j");
        }
    }

    private boolean orderingIsAllowed() {
        if(orders.size() == 3) return false;
        else return true;
    }

    private void placeInBenchProcess() {
        displayCurrentPlayerChampionsInArena();
        System.out.println("choose a champion to place in bench.");
        int choice = Globals.getScanner().nextInt();
        Pair<Champion, Pair<Integer, Integer>> pair = battleArenaManager.playerChampionsCoords(this.getId()).get(choice - 1);
        Champion champion = pair.getKey();
        int i = pair.getValue().getKey();
        int j = pair.getValue().getValue();
        battleArenaManager.removeChampionByCoordinates(champion.getUniqueID(), i, j);
        benchManager.addChampionToBench(champion);
        System.out.println("champion added to bench successfully");
    }

    private void placeInArenaProcess() {
        displayBench();
        System.out.println("choose a champion to place in arena");
        int choice = Globals.getScanner().nextInt();
        Champion champion = benchManager.getBench().getChampions().get(choice - 1);
        benchManager.getBench().getChampions().remove(champion);
        try {
            if (placeChampionInArena(champion))
                System.out.println("champion was added to arena successfully");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("invalid i and j values.");
        }
    }


    public void swapProcess() {
        while (true) {
            System.out.println("Arena champions : ");
            displayCurrentPlayerChampionsInArena();
            //
            System.out.println("Bench Champions : ");
            displayBench();
            //
            System.out.print("From : ");
            String from = Globals.getScanner().next();
            System.out.print("To : ");
            String to = Globals.getScanner().next();


            if (from.equalsIgnoreCase("arena") && to.equalsIgnoreCase("bench")) {

                System.out.println("select a champion from arena: (using numbers)");
                int championFrom = Globals.getScanner().nextInt();

                Pair<Champion, Pair<Integer, Integer>> pair = battleArenaManager.playerChampionsCoords(id).get(championFrom - 1);

                System.out.println("select a champion from bench: (using numbers)");
                int championTo = Globals.getScanner().nextInt();

                Champion championObjectFrom = battleArenaManager.removeChampionByCoordinates(pair.getKey().getUniqueID(), pair.getValue().getKey(), pair.getValue().getValue());
                Champion championObjectTo = benchManager.removeChampionFromBench(benchManager.getBench().getChampions().get(championTo - 1).getUniqueID());

                try {
                    battleArenaManager.placeChampion(championObjectTo, pair.getValue().getKey(), pair.getValue().getValue());
                } catch (OutOfArenaException | InTerrainException ignored) {
                    System.out.println("cannot place champion here");
                    continue;
                }
                benchManager.addChampionToBench(championObjectFrom);
                System.out.println("successfully swapped");
                break;


            } else if (from.equalsIgnoreCase("bench") && to.equalsIgnoreCase("arena")) {

                System.out.println("select a champion from bench: (using numbers)");
                int championTo = Globals.getScanner().nextInt();

                System.out.println("select a champion from arena: (using numbers)");
                int championFrom = Globals.getScanner().nextInt();

                Pair<Champion, Pair<Integer, Integer>> pair = battleArenaManager.playerChampionsCoords(id).get(championFrom - 1);


                Champion championObjectTo = battleArenaManager.removeChampionByCoordinates(pair.getKey().getUniqueID(), pair.getValue().getKey(), pair.getValue().getValue());
                Champion championObjectFrom = benchManager.removeChampionFromBench(benchManager.getBench().getChampions().get(championTo - 1).getUniqueID());

                try {
                    battleArenaManager.placeChampion(championObjectFrom, pair.getValue().getKey(), pair.getValue().getValue());
                } catch (OutOfArenaException | InTerrainException ignored) {

                }
                benchManager.addChampionToBench(championObjectTo);
                System.out.println("successfully swapped");
                break;

            } else {
                System.out.println("Invalid input .. ");
            }
        }
    }

    private void displayBench() {
        int cnt = 1;
        for (Champion c : this.getBenchManager().getBench().getChampions()) {
            System.out.println("(" + cnt + ")" + " " + c);
            cnt++;
        }

    }

    private void purchaseProcess() {
        displayStore();
        while (true) {
            System.out.println("Enter your choice.");
            String choice = Globals.getScanner().next();
            int id;
            try {
                id = storeManager.findChampionByName(choice).getUniqueID();
            } catch (NullPointerException e) {
                System.out.println("Wrong champion name, re-enter your choice.");
                displayStore();
                continue;
            }
            Champion champion = super.purchaseChampion(id);
            if (champion == null) {
                System.out.println("You don't have the money to buy this champion.");
            } else {
                if (!addToMyChampions(champion)) {
                    int returnedCoins = this.getCoins() + this.storeManager.restoreChampion(champion);
                    this.setCoins(returnedCoins);
                }
            }
            break;
        }
    }


    private void sellProcess() {
        System.out.println("Champions in arena:");
        displayCurrentPlayerChampionsInArena();
        System.out.println("\n");
        System.out.println("Champions in bench:");
        displayBench();
        System.out.println("choose 'arena', 'bench', or 'cancel' to cancel");
        String from = Globals.getScanner().next();
        if (from.equalsIgnoreCase("bench")) {
            System.out.println("Enter the number of champion you want to sell.");
            int choice = Globals.getScanner().nextInt();
            Champion sellingChampion = benchManager.getBench().getChampions().get(choice - 1);
            benchManager.removeChampionFromBench(sellingChampion.getUniqueID());
            coins += storeManager.restoreChampion(sellingChampion);

        } else if (from.equalsIgnoreCase("arena")) {
            System.out.println("Enter the number of champion you want to sell.");
            int choice = Globals.getScanner().nextInt();
            Pair<Champion, Pair<Integer, Integer>> championPair = battleArenaManager.playerChampionsCoords(id).get(choice - 1);
            Champion sellingChampion = championPair.getKey();
            int i = championPair.getValue().getKey(), j = championPair.getValue().getValue();
            battleArenaManager.removeChampionByCoordinates(sellingChampion.getUniqueID(), i, j);
            coins += storeManager.restoreChampion(championPair.getKey());

            //respawn the items in the arena randomly.
            for (Item item : sellingChampion.getItems()) {
                battleArenaManager.spawnItem(item);
                sellingChampion = battleArenaManager.getBuffManager().doDeBuffItem(sellingChampion, item);
            }

            sellingChampion.getItems().clear();


        } else if (from.equalsIgnoreCase("cancel")) {
            return;
        }
    }


    /**
     * to add a champion to the player champions list
     *
     * @param champion
     * @return
     */

    private boolean addToMyChampions(Champion champion) {

        //where to add
        while (true) {
            System.out.println("Where to add, arena or bench?");
            String input = Globals.getScanner().next();
            if (input.equalsIgnoreCase("arena")) {
                try {
                    if (placeChampionInArena(champion))
                        return true;
                } catch (IndexOutOfBoundsException e) {
                    System.out.println("invalid i and j values.");
                }


            } else if (input.equalsIgnoreCase("bench")) {
                if (!this.benchManager.addChampionToBench(champion)) {
                    //storeManager.restoreChampion(champion);
                    System.out.println("Champion cannot be added to bench. bench is full.");
                } else {
                    System.out.println("Champion added successfully.");
                    return true;
                }
            } else if (input.equalsIgnoreCase("cancel")) {
                return false;
            } else {
                System.out.println("Invalid place.");
            }
        }

    }

    /**
     * to add champion to arena
     * <p>
     * cases delt with :
     * out or boundaries matrix
     * a full arena
     * invalid square
     *
     * @param champion
     * @return
     */

    private Boolean placeChampionInArena(Champion champion) {
        System.out.println("Enter champion position i & j : (1 to "
                + Globals.getArenaWidth() + ") on the x ax and (1 to "
                + Globals.getArenaHeight() + ") or the y ax.");
        int i = Globals.getScanner().nextInt();
        int j = Globals.getScanner().nextInt();
        i--;
        j--;
        try {
            if (this.battleArenaManager.placeChampion(champion, i, j)) {


                 System.out.println("Champion added successfully.");
                return true;
            } else {
                System.out.println("champion was not added.");
                if (this.battleArenaManager.isFull(this.getId())) {
                    System.out.println("Tile is full.");
                } else if (!this.battleArenaManager.isSquareValid(this.getId(), i, j)) {
                    System.out.println("You already have a champion in this place.");
                }
                return false;
            }
        } catch (OutOfArenaException e) {
            System.out.println("Cannot place champion out of arena.");
            return false;
        } catch (InTerrainException e) {
            System.out.println("Cannot place champion in terrain tile");
            return false;
        }
    }
//
//    private int getNumberOfPurchases() {
//        while(true) {
//            String numberOfPurchases = Globals.getScanner().next();
//            if(numberOfPurchases.equalsIgnoreCase("cancel")) {
//                System.out.println();
//                return -1;
//            }
//            try {
//                int numOfPurchases = Integer.parseInt(numberOfPurchases);
//                if (numOfPurchases > 3 || numOfPurchases < 1) {
//                    System.out.println("You're only allowed to buy up to 3 champions.");
//                    continue;
//                }
//                return numOfPurchases;
//            } catch(Exception e) {
//                System.out.println("re-enter your number of purchases");
//            }
//        }
//    }

    private void displayStore() {
        System.out.println("Current available champions:");
        storeManager.displayVisibleStore();
    }

    /**
     * displays a list of the current player champions so he could choose by index the desired champions to deal with
     */

    private void displayCurrentPlayerChampionsInArena() {
        System.out.println("Current player champions:");

        int cnt = 1;
        System.out.println("ally champions:");
        for (Pair<Champion, Pair<Integer, Integer>> pair : battleArenaManager.playerChampionsCoords(id)) {
            Champion champion = pair.getKey();
            int i = pair.getValue().getKey();
            int j = pair.getValue().getValue();
            TileType tileType = battleArenaManager.getArena().getSquares()[i][j].getType();
            System.out.println("(" + cnt + ")" + " " + champion + " tile: " + tileType.toString());
            cnt++;
        }
        System.out.println();
    }

    /**
     * displays a list of ALL CHAMPIONS IN ARENA based on the current players vision, so he could choose by index an enemy champion to deal with
     */

    private void displayEnemyChampionsInVision() {
        List<Pair<Champion, Pair<Integer, Integer>>> allChampionsInRange = battleArenaManager.getPlayerEnemiesChampionsCoordsInVision(id);
        int cnt = 1;
        System.out.println("visible enemy champions:");
        for (Pair<Champion, Pair<Integer, Integer>> pair : allChampionsInRange) {
            Champion champion = pair.getKey();
            int i = pair.getValue().getKey();
            int j = pair.getValue().getValue();
            TileType tileType = battleArenaManager.getArena().getSquares()[i][j].getType();
            System.out.println("(" + cnt + ")" + " " + champion + " tile: " + tileType.toString());
            cnt++;
        }
    }


    @Override
    public Order collectOrder() {
        displayArena();
        displayCurrentPlayerChampionsInArena();
        System.out.println("Give order to: (id - first number)");
        int choice = Globals.getScanner().nextInt();

        Champion champion = battleArenaManager.playerChampionsCoords(id).get(choice - 1).getKey();
        int championId = champion.getUniqueID();

        if (championHasReachedMaxNumberOfOrders(championId)) {
            System.out.println("Champion " + champion.getName() + " with index " + choice + " has already been ordered 3 times.");
            return null;
        }

        System.out.println("Choose an order: 'Attack', 'Walk', 'Ability':");
        String orderType = Globals.getScanner().next();

        Order order = null;
        while (true) {

            if (orderType.equalsIgnoreCase("attack")) {
                Champion attackerChampion = battleArenaManager.playerChampionsCoords(id).get(choice - 1).getKey();

                System.out.println("Pick a champion to attack:\n");
                displayEnemyChampionsInVision();

                choice = Globals.getScanner().nextInt();
                Champion attackedChampion = battleArenaManager.getPlayerEnemiesChampionsCoordsInVision(id).get(choice - 1).getKey();

                order = new Attack(attackerChampion.getUniqueID(), attackedChampion.getUniqueID());
                break;

            } else if (orderType.equalsIgnoreCase("walk")) {
                Champion walkingChampion = battleArenaManager.playerChampionsCoords(id).get(choice - 1).getKey();
                System.out.println("Set the direction, 'up', 'down', 'left', 'right', or 'cancel' to cancel.");
                String directionChoice = Globals.getScanner().next();
                Direction direction = null;
                if (directionChoice.equalsIgnoreCase("up")) {
                    direction = Direction.UP;
                } else if (directionChoice.equalsIgnoreCase("down")) {
                    direction = Direction.DOWN;
                } else if (directionChoice.equalsIgnoreCase("left")) {
                    direction = Direction.LEFT;
                } else if (directionChoice.equalsIgnoreCase("right")) {
                    direction = Direction.RIGHT;
                } else if (directionChoice.equalsIgnoreCase("cancel")) {
                    return null;
                }

                System.out.println("Set the movement speed, between 0 and " + walkingChampion.getMovementSpeed());
                int movementSpeed = Globals.getScanner().nextInt();

                order = new Walk(walkingChampion.getUniqueID(), movementSpeed, direction);
                break;
            } else if (orderType.equalsIgnoreCase("ability")) {
                //if the player chooses an ability to be performed we have to check whether this ability
                //is being applied on a target or an aoe range
                //once we know that we can start creating our ability order

                int championTargetId = -1;
                if (battleArenaManager.findChampionById(championId).getAbility().getAoe() == 0) {
                    displayCurrentPlayerChampionsInArena();
                    System.out.println("Choose a champion to perform the ability on");
                    choice = Globals.getScanner().nextInt();
                    championTargetId = battleArenaManager.playerChampionsCoords(this.getId()).
                            get(choice - 1).getKey().getUniqueID();
                }
                order = new AbilityOrder(championId, championTargetId);
                break;
            } else if (orderType.equalsIgnoreCase("cancel")) {
                return null;
            }
        }
        return order;
    }

    public void displayArena() {
        battleArenaManager.updateVision(this.id);
        Boolean[][] visionMatrix = battleArenaManager.getArena().getCurrentPlayerVision();
        Square[][] squares = this.getBattleArenaManager().getArena().getSquares();
        for (int i = 0; i < Globals.getArenaWidth(); i++) {

            for (int j = 0; j < Globals.getArenaHeight(); j++) {
                int size = squares[i][j].getChampions().size();

                if (visionMatrix[i][j]) {
                    if(squares[i][j].getType() == TileType.STANDARD) {
                        System.out.print(ColorsPrinting.RESET + "  ");
                    } else if(squares[i][j].getType() == TileType.GRASS) {
                        System.out.print(ColorsPrinting.GREEN + "  ");
                    } else if(squares[i][j].getType() == TileType.TERRAIN) {
                        System.out.print(ColorsPrinting.RED + "  ");
                    } else if(squares[i][j].getType() == TileType.WATER) {
                        System.out.print(ColorsPrinting.BLUE + "  ");
                    }



                    if(size == 0) {
                        System.out.print("*");
                    } else if (size == 1) {
                        System.out.print("C");
                    } else{
                        System.out.print("M");
                    }
                    System.out.print("  " + ColorsPrinting.RESET);
                } else {
                    System.out.print(ColorsPrinting.BLACK + "  *  " + ColorsPrinting.RESET);
                }

//                if(squares[i][j].getItemOnSquare() != null)
//                {
//                    System.out.print("  i  ");
//                }
//                else {
//                    System.out.print(ColorsPrinting.BLUE + "  .  " + ColorsPrinting.RESET);
//                }
            }
            System.out.println();
            System.out.println();
        }

        displayCurrentPlayerChampionsInArena();
        displayEnemyChampionsInVision();
        System.out.println();
    }

    @Override
    public void startExecutionPhase() {
        battleArenaManager.startExecutionPhase(orders);
    }

    @Override
    public void upgradeChampions() {
        List<Champion> upgradableChampions = getListOfUpgradableChampions();
        if(upgradableChampions.size() == 0) {
            return;
        }
        System.out.println("You have " + upgradableChampions.size() + " champion upgrade");

        for (Champion champion : upgradableChampions) {

            for(int i = 0; i < benchManager.getBench().getChampions().size(); i++) {
                Champion benchChampion = benchManager.getBench().getChampions().get(i);
                if(champion.getName().equalsIgnoreCase(benchChampion.getName())
                && champion.getLevel() == benchChampion.getLevel()) {
                    benchManager.getBench().getChampions().remove(benchChampion);
                    i--;
                }
            }

            for(int i = 0; i < Globals.getArenaHeight(); i++) {
                for(int j = 0; j < Globals.getArenaWidth(); j++) {
                    for(int k = 0; k < battleArenaManager.getArena().getSquares()[i][j].getChampions().size(); k++) {
                        Champion arenaChampion = battleArenaManager.getArena().getSquares()[i][j].getChampions().get(k);
                        if(champion.getName().equalsIgnoreCase(arenaChampion.getName())
                                && champion.getLevel() == arenaChampion.getLevel()) {
                            battleArenaManager.getArena().getSquares()[i][j].getChampions().remove(arenaChampion);
                            k--;
                        }
                    }
                }
            }

//            benchManager.removeChampionFromBench(champion.getUniqueID());
//            Pair<Integer, Integer> coords = battleArenaManager.findCoordinates(champion.getUniqueID());
//            if(coords != null) {
//                battleArenaManager.removeChampionByCoordinates(champion.getUniqueID(), coords.getKey(), coords.getValue());
//            }
            System.out.println("Where to put champion? ('arena' or 'bench')");

            //leveling up the champion
            levelUpChampion(champion);

            //placing the champion
            while (true) {
                String choice = Globals.getScanner().next();
                if (choice.equalsIgnoreCase("arena")) {
                    placeChampionInArena(champion);
                    break;
                } else if (choice.equalsIgnoreCase("bench")) {
                    placeChampionInBench(champion);
                    break;
                } else {
                    System.out.println("Error entering your choice, re-enter again.");
                }
            }
        }
    }
}












