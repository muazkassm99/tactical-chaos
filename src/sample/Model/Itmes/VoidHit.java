package sample.Model.Itmes;

import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;

public class VoidHit extends Item {

    VoidHit() {

    }

    public VoidHit(int id)
    {
        super(ItemsClassNames.VOIDHIT.name(), id);
        this.setMaxHealthIncrease(5);

        this.setChampionClass(ChampionClass.VOID);
    }


    @Override
    public String toString() {
        return "Void Hit";
    }

}
