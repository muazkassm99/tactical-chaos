package sample.GUI;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import sample.Model.Enums.ChampionClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Settings {


    private static Stage stage = new Stage();
    private static CheckBox[] checkBoxes= new CheckBox[ChampionClass.values().length + 1];
    private static List<ChampionClass> acceptedClassesList = new ArrayList<>();
    private static Map<String ,Object> map = new HashMap<>();
    private static GridPane gridPane = new GridPane();
    private static VBox vBox = new VBox();

    //comboBoxes
    private static ComboBox<Integer> maxTeam = new ComboBox<>();
    private static ComboBox<Integer> maxRounds = new ComboBox<>();
    private static ComboBox<Integer> maxSwaps = new ComboBox<>();

    //TextFields :
    private static TextField height = new TextField();
    private static TextField width = new TextField();

    public static Map<String, Object> display()
    {


        Button confrim = new Button("ok");

        //Labels :
        Label label = new Label("Arena Size");
        Label label1 = new Label("Max Team Size");
        Label label2 = new Label("Max Rounds Number");
        Label label3 = new Label("Max Swaps in-round");




        //TestFields edits :
        height.setPrefWidth(50);
        height.setPromptText("height");
        width.setPrefWidth(50);
        width.setPromptText("width");

        //For combining TextFields :
        VBox size = new VBox();
        size.setSpacing(5);
        size.getChildren().addAll(height, width);




        /*******************************************************************************************
         * This is where we initialize the comboBoxes with values
         */
        //team size:
        for (int i = 0; i < 9; i++) {
            maxTeam.getItems().add(i+1);
        }
        maxTeam.setValue(4);

        //number of rounds:
        for (int i = 9; i < 18; i++) {
            maxRounds.getItems().add(i+1);
        }
        maxRounds.setValue(18);


        //number of swaps
        maxSwaps.getItems().addAll(1, 2, 3, 4, 5);
        maxSwaps.setValue(2);

        //button edits :
        confrim.setMinWidth(50);



        //GridPane setting up data ..
        GridPane.setConstraints(label, 0, 0);
        GridPane.setConstraints(label1, 0, 1);
        GridPane.setConstraints(label2, 0, 2);
        GridPane.setConstraints(label3, 0, 3);

        GridPane.setConstraints(size, 1, 0);
        GridPane.setConstraints(maxTeam, 1, 1);
        GridPane.setConstraints(maxRounds, 1, 2);
        GridPane.setConstraints(maxSwaps, 1, 3);
        GridPane.setConstraints(confrim, 3, 4);


        /*******************************************************************************************
         * initializing the checkBoxes with values
         *
         */


        //first check is for all
        checkBoxes[0] = new CheckBox("all");
        checkBoxes[0].setSelected(true);

        //other classes:
        for (int i = 0; i < ChampionClass.values().length; i++) {
            checkBoxes[i + 1] = new CheckBox(ChampionClass.values()[i].toString());
            checkBoxes[i + 1].setSelected(false);
        }


        /**
         * this is where we modify checkboxes for canceling selection if (all) is selected
         */
        for (int i = 1; i < checkBoxes.length; i++) {
            int finalI = i;
            checkBoxes[i].setOnAction(e->
            {
                if(checkBoxes[finalI].isSelected())
                {
                    checkBoxes[0].setSelected(false);
                }
            });
        }
        //for the first checkBox
        checkBoxes[0].setOnAction(e->
        {
            if(checkBoxes[0].isSelected())
            {
                for (int i = 1; i < checkBoxes.length; i++) {
                    checkBoxes[i].setSelected(false);
                }
            }
        });


        /*******************************************************************************************
         * making the ckeckBoxes layout
         */

        vBox.getChildren().addAll(checkBoxes);


        vBox.setSpacing(10);
        vBox.setAlignment(Pos.CENTER_LEFT);
        vBox.setMinWidth(130);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(vBox);
        scrollPane.setMaxHeight(300);

        scrollPane.setMinWidth(150);

        /*******************************************************************************************
         * setting the confirm button Action
         */

        confrim.setOnAction(e->
                {

                    onClose();
                }
        );



        /*******************************************************************************************
         * making the comboBoxes layout
         *
         *
         */

        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(20);
        gridPane.setPadding(new Insets(10));


        gridPane.getChildren().addAll(label, label1, label2, label3 );
        gridPane.getChildren().addAll(size, maxTeam, maxRounds, maxSwaps, confrim);


        /*******************************************************************************************
         *
         * the following HBox combines both layouts and is then put into a scene to be displayed
         *
         */


        HBox hBox = new HBox();
        hBox.getChildren().addAll(gridPane, scrollPane);
        hBox.setPadding(new Insets(10));

        stage.setScene(new Scene(hBox));
        stage.setOnCloseRequest(e-> onClose());


        stage.showAndWait();



        /*******************************************************************************************
         * returns the map which has all the settings values
         */

        return map;
    }


    private static void onClose()
    {
        if (checkBoxes[0].isSelected()) {
            for (ChampionClass c : ChampionClass.values()) {
                acceptedClassesList.add(c);
            }
        } else {

            for (int i = 1; i < checkBoxes.length ; i++) {
                if (checkBoxes[i].isSelected()) {
                    acceptedClassesList.add(ChampionClass.valueOf(checkBoxes[i].getText()));
                }
            }
        }

        int heightNumeric , widthNumberic;
        try{
            heightNumeric = Integer.parseInt(height.getText());
            widthNumberic = Integer.parseInt(width.getText());
        }catch (Exception ee)
        {
            heightNumeric = 20;
            widthNumberic = 20;
        }

        map.put("height", heightNumeric);
        map.put("width", widthNumberic);
        map.put("maxTeam", maxTeam.getValue());
        map.put("maxSwaps", maxSwaps.getValue());
        map.put("maxRounds", maxRounds.getValue());
        map.put("acceptedClasses", acceptedClassesList);

        stage.close();
    }

}