package sample.GUI;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Pair;
import sample.GUI.Dialogs.AlertBox;
import sample.GUI.Dialogs.CoordsSelector;
import sample.GUI.Dialogs.PlacePicker;
import sample.GUI.Dialogs.TakeAction;
import sample.Model.Champion;
import sample.Model.PrimitiveModels.Square;
import sample.Utilities.Globals;
import sample.View.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StartGame {

    private Integer height , width;
    private GridPane gridPane;
    private BorderPane borderPane;
    private Stage stage;
    private Player player;


    private int avalibaleSwaps = Globals.getMaxNumberOfSwaps();

    private Champion championSwap1, championSwap2;
    private Pair<Integer, Integer> oldCoords;
    private String from;


    private int purchasingTimes = Globals.getMaxNumberOfPurchasesInRound();

    public StartGame(Stage primaryStage, Player player)
    {
        this.player = player;


        gridPane = new GridPane();
        height = Globals.getArenaHeight();
        width = Globals.getArenaWidth();
        borderPane = new BorderPane();
        stage = primaryStage;


        championSwap1 = null;
        championSwap2 = null;
        from = "";



        updateAll();
        initStoreLayout();

        borderPane.setLeft(renderTiers());


    }

    /**
     * calls all the update funtions.
     */
    private void updateAll()
    {
        updateStats();
        updateBench();
        updateArena();
        updateStore();
    }

    public void start()
    {

        Scene scene = new Scene(borderPane);

        stage.setScene(scene);
        stage.setMaximized(true);

        stage.show();

    }

    Node renderTiers() {

        HBox tiers= new HBox();
        tiers.setMinWidth(125);
        return tiers;
    }


    //*************************************           Stats          ************************************


    private void updateStats()
    {
        Label id = new Label("id : "+this.player.getId());
        Label coins = new Label("coins : " + this.player.getCoins());
        Button terminate = new Button("Terminate");
        terminate.setOnAction(e->stage.close());
        renderStats(id, coins, terminate);
    }

    private void renderStats(Label id, Label coins, Button terminate) {
        HBox hBox = new HBox();
        hBox.getChildren().addAll(id, coins, terminate);
        hBox.setSpacing(20);
        hBox.setAlignment(Pos.CENTER);

        borderPane.setTop(hBox);
    }





    //****************************************          ARENA          ************************************

    /**
     * updates the arena and takes in the input from the user and does the action then refreshes.
     */
    private void updateArena()
    {
        Button[][] buttons = new Button[height][width];

        Square[][] squares = this.player.getBattleArenaManager().getArena().getSquares();
        this.player.getBattleArenaManager().updateVision(this.player.getId());
        Boolean[][] vision = this.player.getBattleArenaManager().getArena().getCurrentPlayerVision();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {

                if(vision[i][j] == true)
                {
                    List<Champion> champions = squares[i][j].getChampions();
                    if(champions.size() > 1) // multiple champions
                        buttons[i][j] = new Button("M");
                    else if (champions.size() == 1) // only one champion
                        buttons[i][j] = new Button(champions.get(0).getName() + " ID : " + champions.get(0).getUniqueID() );
                    else { //no champions but can see
                        buttons[i][j] = new Button("");
                    }

                }
                else
                {
                    buttons[i][j] = new Button("x");
                }

                buttons[i][j].setMinSize(100, 100);
                buttons[i][j].setMaxSize(100, 100);


                int finalI = i;
                int finalJ = j;
                buttons[i][j].setOnAction(e->
                {
                    Pair<Champion, Integer> modifyingFromArena = DisplaySquareChampion.showChampions(squares[finalI][finalJ].getChampions(), this.player, finalI, finalJ);
                    if(modifyingFromArena != null) {
                        Champion champion = modifyingFromArena.getKey();

                        int action = modifyingFromArena.getValue();

                        if (action == 1) {

                            //already deleted
                            this.player.setCoins(this.player.getCoins() + this.player.getStoreManager().restoreChampion(champion));
                            updateAll();

                            //no way we return

                        } else if (action == 2) {
                            //already deleted
                            if (!this.player.placeChampionInBench(champion)) {
                                //return the champion
                                this.player.placeChampionInArena(champion, finalI, finalJ);
                            }
                            updateAll();
                        } else // swap
                        {
                            if (avalibaleSwaps > 0) {
                                //already deleted
                                oldCoords = new Pair(finalI, finalJ);

                                if (championSwap1 == null) {
                                    championSwap1 = champion;
                                    from = "arena";
                                } else if (championSwap2 == null) {
                                    championSwap2 = champion;
                                    doSwap();
                                }
                                avalibaleSwaps--;
                                updateAll();
                            } else {//return the champion to arena
                                this.player.placeChampionInArena(champion, finalI, finalJ);
                            }
                        }

                    }


                    updateAll();
                });

                GridPane.setConstraints(buttons[i][j], j, i);
            }
        }

        renderArena(buttons);
    }

    /**
     * just shows the arena
     * @param buttons
     */
    private void renderArena(Button[][] buttons)
    {

        gridPane.setBackground(new Background(new BackgroundFill(Color.PINK, CornerRadii.EMPTY, Insets.EMPTY)));

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                gridPane.getChildren().add(buttons[i][j]);
            }
        }

        gridPane.setVgap(10);
        gridPane.setHgap(10);

        gridPane.setAlignment(Pos.CENTER);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(gridPane);

        borderPane.setCenter(scrollPane);
    }


    //****************************************          STORE          ************************************


    /**
     * for getting the number of purchases.
     * takes between 0 and 3 number or it will re-render.
     */


    private void initStoreLayout()
    {
        TextField textField = new TextField();
        textField.setPromptText("number of purchases");

        Button button = new Button("ok");
        button.setOnAction(e->
        {

            try{
                purchasingTimes = Integer.parseInt(textField.getText());
                if(purchasingTimes  <= Globals.getMaxNumberOfPurchasesInRound() && purchasingTimes >= 0) {
                    updateStore();
                }
                else {
                    textField.setText("");
                    textField.setPromptText("0 -> 3 only");
                }
            }
            catch (Exception ex)
            {
                initStoreLayout();
            }
        });

        VBox vBox = new VBox();
        vBox.getChildren().addAll(textField, button);
        vBox.setSpacing(20);
        vBox.setAlignment(Pos.CENTER);
        vBox.setMinWidth(100);

        borderPane.setRight(vBox);
    }

    /**
     * for updating the store.
     * here is the place where the purchasing happens.
     * delt with cases :
     *  1-bench or arena is full -> restore champion
     *  2-
     */

    private void updateStore()
    {

        if(purchasingTimes <= 0) {
            borderPane.setRight(null);
            return;
        }


        List<Champion> champions= this.player.getStoreManager().getVisibleChampions();
        Button[] championsBtnsOnStore = new Button[champions.size()];

        for (int i = 0; i < champions.size(); i++) {


            championsBtnsOnStore[i] = new Button(champions.get(i).getName() + " (" +  champions.get(i).getPrice() + ")");
            championsBtnsOnStore[i].setMinSize(75, 75);
            int finalI = i;
            championsBtnsOnStore[i].setOnAction(e->{

                Champion champion = null;
                champion = this.player.purchaseChampion(champions.get(finalI).getUniqueID()); // does the sellMe
                if(champion != null) {
                        boolean bench = PlacePicker.putInBench();

                        if(bench) // remove champion from store and place it in bench
                        {
                            if(!this.player.placeChampionInBench(champion)){ // player cant place in bench
                                this.player.setCoins(this.player.getCoins() + this.player.getStoreManager().restoreChampion(champion)); //return the champion to store.
                            }else{
                                purchasingTimes-=1;
                            }
                        }else   // remove champion from store and place it in Arena
                        {
                            Pair<Integer, Integer> coords = CoordsSelector.select();
                            if(!this.player.placeChampionInArena(champion, (coords.getKey()-1), (coords.getValue())-1)) {
                                this.player.setCoins(this.player.getCoins() + this.player.getStoreManager().restoreChampion(champion));
                            }else{
                                purchasingTimes-=1;
                            }
                        }



                    updateAll();
                }else { // no enough money
                        AlertBox.display("Error", "You don't have enough money!!");
                }



            });
        }
        borderPane.setRight(renderStore(championsBtnsOnStore));
    }

    /**
     * for creating a store layout
     * also contains a button for canceling the process.
     *
     * @param buttons
     * @return
     */
    Node renderStore(Button[] buttons)
    {

        VBox vBox = new VBox(20);
        for (Button b :
                buttons) {
            vBox.getChildren().add(b);
        }


        //canceling button
        Button cancel = new Button("Cancel");
        cancel.setOnAction(e-> borderPane.setRight(null));



        vBox.getChildren().add(cancel);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(vBox);
        scrollPane.setPadding(new Insets(10));
        return  scrollPane;
    }


    //**************************************        Bench             ************************************************



    /**
     * for updating the bench
     */
    private void updateBench()
    {
        List<Champion> champions = this.player.getBenchManager().getBenchChampions();
        Button[] championsBtnsOnBench = new Button[champions.size()];

        for (int i = 0; i < champions.size(); i++) {

            Champion champion = champions.get(i);
            int championId = champion.getUniqueID();
            championsBtnsOnBench[i] = new Button(champion.getName() + " id : " + championId);
            championsBtnsOnBench[i].setMinSize(150, 150);
            championsBtnsOnBench[i].setOnAction(e->{

                int actionTaken = TakeAction.takeActionBench();

                if(actionTaken == 1) // sell champion
                {

                    Champion champion1 = this.player.getBenchManager().removeChampionFromBench(championId);
                    player.setCoins(player.getCoins() + this.player.getStoreManager().restoreChampion(champion1));
                    updateAll();
                }

                else if (actionTaken == 2) // to arena
                {
                    Pair<Integer, Integer> coords = CoordsSelector.select();

                    //remove champion from bench
                    Champion champion1 = this.player.getBenchManager().removeChampionFromBench(championId);
                    if(!this.player.placeChampionInArena(champion1, (coords.getKey()-1), (coords.getValue())-1))
                    {
                        this.player.getBenchManager().addChampionToBench(champion1);
                    }
                    updateAll();
                }
                else { // swap

                    if(avalibaleSwaps >0) {
                        Champion champion1 = this.player.getBenchManager().removeChampionFromBench(championId);

                        if (championSwap1 == null) {
                            championSwap1 = champion1;
                            from = "bench";
                        } else if (championSwap2 == null) {
                            championSwap2 = champion1;
                            doSwap();
                        }
                        avalibaleSwaps--;
                        updateAll();
                    }

                }

            });

        }
        borderPane.setBottom(renderBench(championsBtnsOnBench));
    }



    /**
     * for creating a layout for the bench
     * @param buttons
     * @return
     */
    Node renderBench(Button[] buttons)
    {
        HBox hBox = new HBox(20);
        for (Button b : buttons) {
            hBox.getChildren().add(b);
        }

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(hBox);
        scrollPane.setMinHeight(100);
        scrollPane.setPadding(new Insets(10));

        return scrollPane;

    }


    //*******************************************       SWAPS                 **********************************

    /**
     * does the swapping using the global champions and the global String and the global pair of coordinates.
     *
     *     private Champion championSwap1, championSwap2;
     *     private Pair<Integer, Integer> oldCoords;
     *     private String from;
     */
    private void doSwap() {
        if(championSwap1 != null && championSwap2 != null) {
            if (from.equalsIgnoreCase("bench")) {

                player.placeChampionInArena(championSwap1, oldCoords.getKey(), oldCoords.getValue());
                player.placeChampionInBench(championSwap2);

            } else if (from.equalsIgnoreCase("arena")) {

                player.placeChampionInBench(championSwap1);
                player.placeChampionInArena(championSwap2, oldCoords.getKey(), oldCoords.getValue());
            }
        }
        championSwap1 = null;
        championSwap2 = null;
        oldCoords = null;
        from = "";
        updateAll();
    }


    public void setPlayer(Player player)
    {
        this.player = player;
        updateAll();
        initStoreLayout();
    }
}
