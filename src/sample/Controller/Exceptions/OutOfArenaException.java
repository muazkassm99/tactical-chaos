package sample.Controller.Exceptions;

public class OutOfArenaException extends Exception {
    public OutOfArenaException() {
    }

    public OutOfArenaException(String message) {
        super(message);
    }
}
