package sample.Model.PrimitiveModels;

import sample.Model.Champion;

import java.util.List;

public class Store {
    private List<Champion> champions;

    public Store() {

    }

    public List<Champion> getChampions() {
        return champions;
    }

    public void setChampions(List<Champion> champions) {
        this.champions = champions;
    }
}
