package sample.View;

import javafx.util.Pair;
import sample.Controller.BattleArenaManager;
import sample.Controller.BenchManager;
import sample.Controller.Exceptions.InTerrainException;
import sample.Controller.Exceptions.OutOfArenaException;
import sample.Controller.SaveGameCallBack;
import sample.Model.Champion;
import sample.Model.Enums.ChampionPlace;
import sample.Controller.StoreManager;
import sample.Model.Itmes.Item;
import sample.Model.OrderAttributes.Order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Player {
    int id;
    int coins;

    BattleArenaManager battleArenaManager;
    BenchManager benchManager;
    StoreManager storeManager;
    List<Order> orders;
    transient SaveGameCallBack saveGameCallBack;

    /*
    protected Map<Pair<String, Integer>, Integer> championsLevelCounter;
    protected Map<ChampionClass, Integer> championsTierCounter;
    */

    private String playerType;

    Player() {

    }

    Player(String playerType) {
        this.playerType = playerType;
        this.coins = 0;
        this.battleArenaManager = new BattleArenaManager();
        this.benchManager = new BenchManager();
        this.storeManager = new StoreManager();
        this.orders = new ArrayList<>();
    }

    Player(String playerType, int id, SaveGameCallBack saveGameCallBack) {
        this(playerType);
        this.id = id;
        this.saveGameCallBack = saveGameCallBack;
    }

    Player(String playerType, int id, SaveGameCallBack saveGameCallBack, StoreManager storeManager) {
        this(playerType, id, saveGameCallBack);
        this.storeManager = storeManager;
    }

    /**
     * this phase will implement the following concepts based on the user ui:
     * -show who's turn
     * -show coins
     * -get number of purchases
     * -show store champions
     * -purchase from store
     * -place bought champion
     * -place champion from bench to arena
     * -place champion from arena to bench
     * -swap champions
     * -give an order to champion if allowed
     */
    public abstract void startPlanningPhase(boolean beforeNine);

    public void saveGame() {
        saveGameCallBack.onSaveGame();
    }

    public boolean championHasReachedMaxNumberOfOrders(int championId) {
        int cnt = 0;
        for (Order order : orders) {
            if (order.getChampionId() == championId) {
                cnt++;
                if(cnt == 3) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * this phase will show the results of the game based on the user ui:
     */


    public abstract void startExecutionPhase();


    /**
     * this functionality does the process of charging coins and
     * removing the champion you want to buy from the visible
     * champions list in the store
     * <p>
     * and it returns true when this process is done
     * and it returns false otherwise
     *
     * @param championId
     * @return
     */

    public Champion purchaseChampion(int championId) {
        Champion inHandChampion = storeManager.findChampionById(championId);

        if (chargeCoins(inHandChampion.getPrice()) == true) {
            storeManager.sellMe(championId);
            inHandChampion.setPlayerId(this.id);
            return inHandChampion;
        }
        return null;

    }

    public boolean placeChampionInBench(Champion champion) {
        if (benchManager.addChampionToBench(champion)) {
            champion.setPlace(ChampionPlace.BENCH);
            return true;
        }
        return false;
    }

    public boolean placeChampionInArena(Champion champion, int i, int j) {
        try {
            if (battleArenaManager.placeChampion(champion, i, j)) {
                champion.setPlace(ChampionPlace.BATTLE_ARENA);
                return true;
            }
        } catch (OutOfArenaException | InTerrainException ignored) {

        }
        return false;
    }

    public BattleArenaManager getBattleArenaManager() {
        return battleArenaManager;
    }

    public void setBattleArenaManager(BattleArenaManager battleArenaManager) {
        this.battleArenaManager = battleArenaManager;
    }

    public boolean chargeCoins(int value) {
        if (value <= this.coins) {
            this.coins -= value;
            return true;
        }
        return false;
    }

    abstract public Order collectOrder();

    public boolean swapChampions(Champion champion1, Champion champion2) {


        //IF BOTH CHAMPS ARE IN THE SAME AREA.
        if ((champion1.getPlace() == ChampionPlace.BENCH && champion2.getPlace() == ChampionPlace.BENCH) ||
                (champion1.getPlace() == ChampionPlace.BATTLE_ARENA && champion2.getPlace() == ChampionPlace.BATTLE_ARENA)
        ) {
            return false;
        }


        if (champion2.getPlace() == ChampionPlace.BENCH) // the second one is on Bench
        {
            Champion temp = champion1;
            champion1 = champion2;
            champion2 = temp;
        }

        //REMOVING BOTH CHAMPS
        Champion temp1 = this.benchManager.removeChampionFromBench(champion1.getUniqueID());
        Pair<Integer, Integer> coords = this.battleArenaManager.findCoordinates(champion2.getUniqueID());
        Champion temp2 = this.getBattleArenaManager().removeChampionByCoordinates(champion2.getUniqueID(),
                coords.getKey(), coords.getValue());

        //ADDING CHAMPS
        try {
            this.battleArenaManager.placeChampion(temp1, coords.getKey(), coords.getValue());
        } catch (OutOfArenaException | InTerrainException ignored) {

        }
        this.getBenchManager().addChampionToBench(temp2);

        return true;

    }

    public List<Champion> getListOfUpgradableChampions() {
        List<Champion> battleArenaChampions = battleArenaManager.getBattleArenaChampions(id);
        List<Champion> benchChampions = benchManager.getBenchChampions();
        Map<Pair<String, Integer>, Integer> playerChampionsMap = new HashMap<>();
        for (Champion champion : battleArenaChampions) {
            int level = champion.getLevel();
            Pair<String, Integer> pair = new Pair<>(champion.getName(), level);
            if (playerChampionsMap.containsKey(pair)) {
                playerChampionsMap.put(pair, playerChampionsMap.get(pair) + 1);
            } else {
                playerChampionsMap.put(pair, 1);
            }
        }
        for (Champion champion : benchChampions) {
            int level = champion.getLevel();
            Pair<String, Integer> pair = new Pair(champion.getName(), level);
            if (playerChampionsMap.containsKey(pair)) {
                playerChampionsMap.put(pair, playerChampionsMap.get(pair) + 1);
            } else {
                playerChampionsMap.put(pair, 1);
            }
        }
        List<Champion> upgradableChampions = new ArrayList<>();

        map:
        for (Map.Entry<Pair<String, Integer>, Integer> entry : playerChampionsMap.entrySet()) {
            if (entry.getValue() == 3) {
                for (Champion champion : battleArenaChampions) {
                    if (champion.getName().equalsIgnoreCase(entry.getKey().getKey())) {
                        upgradableChampions.add(champion);
                        break map;
                    }
                }
                for (Champion champion : benchChampions) {
                    if (champion.getName().equalsIgnoreCase(entry.getKey().getKey())) {
                        upgradableChampions.add(champion);
                        break map;
                    }
                }
            }
        }
        return upgradableChampions;
    }

    public void levelUpChampion(Champion champion) {
        if (champion.getLevel() == 1) {
            int championHealth = champion.getHealth();
            int championHealthBuff = (championHealth * 20) / 100;
            champion.setCurrentHealth(championHealth + championHealthBuff);
            int championBasicAttackDamage = champion.getBasicAttackDamage();
            int championBasicAttackDamageBuff = (championBasicAttackDamage * 10) / 100;
            champion.setBasicAttackDamage(championBasicAttackDamage + championBasicAttackDamageBuff);
            champion.setArmor(champion.getArmor() + 20);
            champion.setMagicResist(champion.getMagicResist() + 20);
            champion.setLevel(champion.getLevel() + 1);
        } else if (champion.getLevel() == 2) {
            //leveling up the champion
            int championHealth = champion.getHealth();
            int championHealthBuff = (championHealth * 25) / 100;
            champion.setCurrentHealth(championHealth + championHealthBuff);
            int championBasicAttackDamage = champion.getBasicAttackDamage();
            int championBasicAttackDamageBuff = (championBasicAttackDamage * 15) / 100;
            champion.setBasicAttackDamage(championBasicAttackDamage + championBasicAttackDamageBuff);
            champion.setArmor(champion.getArmor() + 25);
            champion.setMagicResist(champion.getMagicResist() + 25);
            champion.setLevel(champion.getLevel() + 1);
        }
    }


    public Item sellItem(Champion champion, Item item)
    {
        for (Item i: champion.getItems()) {
            if(i.getUniqueID() == item.getUniqueID())
            {
               champion.getItems().remove(item);
                return item;
            }
        }
        return null;
    }

    public abstract void upgradeChampions();

    public BenchManager getBenchManager() {
        return benchManager;
    }

    public void setBenchManager(BenchManager benchManager) {
        this.benchManager = benchManager;
    }

    public StoreManager getStoreManager() {
        return storeManager;
    }

    public void setStoreManager(StoreManager storeManager) {
        this.storeManager = storeManager;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void removeDeadChampions() {
        battleArenaManager.removeDeadChampions();
    }

    public void setSaveGameCallBack(SaveGameCallBack saveGameCallBack) {
        this.saveGameCallBack = saveGameCallBack;
    }
}
