package sample.Model.PrimitiveModels;

import sample.Utilities.Globals;

public class BattleArena {
    private Square[][] squares;
    private Boolean[][] currentPlayerVision;

    BattleArena() {

    }

    public BattleArena(int height, int width) {
        squares = new Square[height][width];

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                squares[i][j] = new Square();
            }
        }

        currentPlayerVision = new Boolean[width][height];
    }

    public Square[][] getSquares() {
        return squares;
    }

    public void setSquares(Square[][] squares) {
        this.squares = squares;
    }

    public Boolean[][] getCurrentPlayerVision() {
        return currentPlayerVision;
    }

    public void setCurrentPlayerVision(Boolean[][] currentPlayerVision) {
        this.currentPlayerVision = currentPlayerVision;
    }
}