package sample.Controller;

import sample.Controller.Filter.AcceptedClassesFilter;
import sample.Controller.Filter.Filter;
import sample.Model.Ability;
import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;
import sample.Model.Enums.ChampionPlace;
import sample.Model.PrimitiveModels.Store;
import sample.Utilities.Globals;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/*
    FOR MANAGING BUYING AND SELLING CHAMPIONS

 */


public class StoreManager {
    private Store store;
    private List<Champion> visibleChampions;//the n champs a player can purchase from.
    private transient Filter filter;

    public StoreManager() {
        store = new Store();
        filter = new AcceptedClassesFilter();
        store.setChampions(generateStoreChampionList());
        visibleChampions = new ArrayList<>();
        visibleChampions = refreshAvailableChampions();

    }

    //************************************************************************

    /**
     * will return the cost of the champion the player wants to sell
     * if the champion is not damaged then the money returned will be
     * the same as he bought it
     * putting into consideration if the champion is level 2 his price
     * will be x3 and if he is level 3 his price will be x9
     *
     * @param champion the champion we want to restore
     * @return the coins value
     */
    public int restoreChampion(Champion champion) {
        boolean newChampion = false;
        int coinsToBeReturned = 0;

        //CHECKING FOR DAMAGED ATTRIBUTES
        if (champion.getCurrentHealth() == champion.getHealth() &&
                champion.getCurrentMana() == champion.getManaCost()) {
            newChampion = true;
        }

        if (champion.getLevel() == 1) {
            coinsToBeReturned = champion.getPrice();
        } else if (champion.getLevel() == 2) {
            coinsToBeReturned = champion.getPrice() * 3;
        } else {
            coinsToBeReturned = champion.getPrice() * 9;
        }

        //REFACTORING THE CHAMPION
        champion.setCurrentMana(champion.getManaCost());
        champion.setCurrentHealth(champion.getHealth());
        champion.setPlace(ChampionPlace.STORE);


        //RE-ADD THE CHAMPION TO THE STORE
        List<Champion> champions = this.getVisibleChampions();
        champions.add(champion);
        champion.setPlayerId(0);
        this.setVisibleChampions(champions);


        return (newChampion) ? coinsToBeReturned : coinsToBeReturned / 2;

    }



    //************************************************************************

    /**
     * This functionality removes the champion from the visible
     * store and returns the champion.
     * the deletion will be done depending on the unique champion id.
     * @param championId the unique champion id.
     * @return the champion sold from the visible store.
     */
    public Champion sellMe(int championId) {
        Champion championReturn = null;

        for (Champion c : visibleChampions) {
            if (c.getUniqueID() == championId) {
                championReturn = c;
                visibleChampions.remove(c);
                break;
            }
        }
        return championReturn;
    }



    //************************************************************************

    /**
     * this functionality returns the champion using his UniqueID
     *
     * BUT IT DOES NOT REMOVE IT FROM THE LIST .. IT ONLY RETURNS
     * THE SAME OBJECT YOU ARE LOOKING FOR.
     *
     * @param championId the UniqueID of a champion.
     * @return the champion in the visible store.
     */


    public Champion findChampionById(int championId) {
        Champion returnedChampion = null;
        for (Champion champion : visibleChampions) {
            if (championId == champion.getUniqueID()) {
                returnedChampion = champion;
            }
        }
        return returnedChampion;
    }



    //************************************************************************

    public Champion findChampionByName(String championName) {
        Champion returnedChampion = null;
        for (Champion champion : visibleChampions) {
            if (champion.getName().equalsIgnoreCase(championName)) {
                returnedChampion = champion;
            }
        }
        return returnedChampion;
    }


    //************************************************************************





    /**
     * IT DOES NOT REMOVE IT FROM THE VISIBLE STORE.
     * @return the cheapest champion in the visible store list
     */
    public Champion getCheapestChampionInVisibleStore() {
        int cheapestPrice = 100;
        Champion cheapestChampion = null;
        for (Champion champion : this.visibleChampions) {
            if (champion.getPrice() < cheapestPrice) {
                cheapestPrice = champion.getPrice();
                cheapestChampion = champion;
            }
        }
        return cheapestChampion;
    }



    //************************************************************************

    /**
     * this functionality updates the visible champions list
     * in this class. And the champions it brings from the store
     * are deleted from there.
     * And when it is casted again the old ones are restored.
     *
     * @return a list of champions to buy from.
     */
    public List<Champion> refreshAvailableChampions() {

        //GET THE STORE CHAMPIONS
        List<Champion> storeChampions = store.getChampions();

        /* RETURN THE EXISTING CHAMPIONS TO THE STORE */
        if (visibleChampions.size() > 0)
            for (Champion c : visibleChampions) {
                storeChampions.add(c);
            }

        //CLEAR THE CURRENT LIST
        visibleChampions.clear();

        Random random = new Random();

        /*
        RE-CREATE A NEW LIST RANDOMLY AND DELETE
          * *THE CHOSEN CHAMPIONS FROM THE STORE LIST
        */
        for (int i = 0; i < Globals.getNumberOfVisibleChampions(); i++) {
            int randomIndex = Math.abs(random.nextInt(storeChampions.size() - 1));
            visibleChampions.add(storeChampions.remove(randomIndex));
        }

        store.setChampions(storeChampions);
        return visibleChampions;
    }



    //************************************************************************

    /**
     * generates a list of champions using the CSV file
     *
     * @return the list of champions generated from the file.
     */
    public List<Champion> generateStoreChampionList() {
        List<Champion> champions = new ArrayList<>();
        try {
            int uniqueID = 1;   //THIS ID IDENTIFIES EACH CHAMPION FROM THE OTHER CHAMPIONS.
            for (int i = 0; i < 10; i++) {

                Scanner scanner = new Scanner(new File(
                        //"C:\\Users\\Ahmad\\Desktop\\Tactical Chaos (ahmad edit 2-1-2019)\\Tactical Chaos\\src\\sample\\champions.csv"));
//                        "C:\\Users\\muazk\\Desktop\\Tactical Chaos\\src\\sample\\champs.csv"));
//                        "C:\\Users\\Ahmad\\Desktop\\Tactical Chaos\\src\\sample\\champs.csv"));
                        "C:\\Users\\muazk\\Desktop\\Java project\\Tactical Chaos (ahmad edit 4-1-2020)\\Tactical Chaos\\src\\sample\\champions.csv"));


                while (scanner.hasNext()) {
                    String line = scanner.nextLine();
                    String[] words = line.split(",");

                    String name = words[0];

                    List<ChampionClass> classes = new ArrayList<>();
                    String championClass = words[1];
                    for (ChampionClass champClass : ChampionClass.values()) {
                        if (champClass.toString().equalsIgnoreCase(championClass)) {
                            classes.add(champClass);
                        }
                    }
                    championClass = words[2];
                    if (!championClass.equals("-")) {
                        for (ChampionClass champClass : ChampionClass.values()) {
                            if (champClass.toString().equalsIgnoreCase(championClass)) {
                                classes.add(champClass);
                            }
                        }
                    }
                    championClass = words[3];
                    if (!championClass.equals("-")) {
                        for (ChampionClass champClass : ChampionClass.values()) {
                            if (champClass.toString().equalsIgnoreCase(championClass)) {
                                classes.add(champClass);
                            }
                        }
                    }

                    int level = 1;
                    int health = Integer.parseInt(words[5]);
                    int armor = Integer.parseInt(words[6].replaceAll("%", ""));
                    int manaCost = Integer.parseInt(words[15]);
                    int currentMana = Integer.parseInt(words[14]);
                    int magicResist = Integer.parseInt(words[7].replaceAll("%", ""));
                    int visionRange = Integer.parseInt(words[8]);
                    int attackRange = Integer.parseInt(words[9]);
                    int movementSpeed = Integer.parseInt(words[11]);
                    int basicAttackDamage = Integer.parseInt(words[10]);
                    int criticalStrikeDamage = Integer.parseInt(words[6].replaceAll("%", ""));
                    int criticalStrikeChance = Integer.parseInt(words[6].replaceAll("%", ""));
                    int price = Integer.parseInt(words[4]);

                    visionRange = (Math.min(Globals.getArenaHeight(), Globals.getArenaWidth()) * visionRange) / 100;
                    attackRange = (Math.min(Globals.getArenaHeight(), Globals.getArenaWidth()) * attackRange) / 100;
                    movementSpeed = (Math.min(Globals.getArenaHeight(), Globals.getArenaWidth()) * movementSpeed) / 100;

                    Champion champion = new Champion(
                            uniqueID++, name,
                            level,
                            health,
                            classes,
                            armor,
                            manaCost,
                            currentMana,
                            magicResist,
                            visionRange,
                            attackRange,
                            movementSpeed,
                            basicAttackDamage,
                            criticalStrikeDamage,
                            criticalStrikeChance,
                            price
                    );
                    champion.setPlayerId(0);

                    int aoe = Integer.parseInt(words[16]);
                    int behindTheTarget = Integer.parseInt(words[17]);
                    boolean boolBehindTheTarget = false;
                    if(behindTheTarget == 1)
                        boolBehindTheTarget = true;
                    int inLine = Integer.parseInt(words[18]);
                    boolean boolInLine = false;
                    if(inLine == 1)
                        boolInLine = true;
                    int damage = Integer.parseInt(words[19]);
                    int percentDamage = Integer.parseInt(words[20]);
                    int trueDamage = Integer.parseInt(words[21]);
                    int stun = Integer.parseInt(words[22]);
                    int armorResistReduce = Integer.parseInt(words[23]);
                    int magicResistReduce = Integer.parseInt(words[24]);
                    int attackDamageReduce = Integer.parseInt(words[25]);
                    int damageAsMissingHealth = Integer.parseInt(words[26]);
                    boolean boolDamageAsMissingHealth = false;
                    if(damageAsMissingHealth == 1) {
                        boolDamageAsMissingHealth = true;
                    }
                    int selfHeal = Integer.parseInt(words[27]);
                    int allyHeal = Integer.parseInt(words[28]);
                    int buffRange = Integer.parseInt(words[29]);
                    int buffDamage = Integer.parseInt(words[30]);
                    int buffMana = Integer.parseInt(words[31]);

                    Ability ability = new Ability(uniqueID, aoe, boolBehindTheTarget, boolInLine, damage, percentDamage, trueDamage, stun,
                    armorResistReduce, magicResistReduce, attackDamageReduce, boolDamageAsMissingHealth, false, selfHeal, allyHeal,
                            buffRange, buffDamage, buffMana);

                    champion.setAbility(ability);

                    if (filter.isAccepted(champion)) {
                        champion.setPlace(ChampionPlace.STORE);
                        champions.add(champion);
                    }

                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return champions;
    }


    //************************************************************************


    public void refresh()
    {
        this.setVisibleChampions(this.refreshAvailableChampions());
    }




    //************************************************************************
    //************************************************************************

    /**
     * for displaying the visible store.
     */

    public void displayVisibleStore()
    {
        for (Champion c : this.getVisibleChampions()) {
            System.out.println(c);
        }
    }


    /**
     * setters and getters ....
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     */




    public List<Champion> getVisibleChampions() {
        return visibleChampions;
    }

    public void setVisibleChampions(List<Champion> visibleChampions) {
        this.visibleChampions = visibleChampions;
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
}

