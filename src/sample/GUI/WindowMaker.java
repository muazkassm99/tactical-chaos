package sample.GUI;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class WindowMaker {
    public static void makeMeAWindow(Scene scene, String title, Stage stage, Boolean wait)
    {
        stage.setScene(scene);
        stage.setTitle(title);

        if(wait)
        {
            stage.showAndWait();
        }
        else
        {
            stage.show();
        }

    }
}
