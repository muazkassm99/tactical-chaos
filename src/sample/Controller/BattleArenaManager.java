package sample.Controller;

import javafx.util.Pair;
import sample.Controller.Exceptions.InTerrainException;
import sample.Controller.Exceptions.OutOfArenaException;
import sample.Model.Ability;
import sample.Model.Enums.*;
import sample.Model.Itmes.*;
import sample.Model.OrderAttributes.AbilityOrder;
import sample.Model.OrderAttributes.Attack;
import sample.Model.OrderAttributes.Order;
import sample.Model.OrderAttributes.Walk;
import sample.Model.Enums.ChampionStatus;
import sample.Model.PrimitiveModels.BattleArena;
import sample.Model.Champion;
import sample.Model.PrimitiveModels.Square;
import sample.Utilities.Globals;
import sample.Utilities.OrderThread;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static sample.Model.Enums.Direction.UP;

public class BattleArenaManager {
    private BattleArena arena;
    private OrderManager orderManager;
    private BuffManager buffManager;


    /**
     * this method iterates over all champions except the current player champion that are in vision and adds
     * them to a list.
     *
     * @param playerId the current player's id so we can differentiate enemies from allies.
     * @return list of the objects champions with its corresponding coordinates.
     */

    public List<Pair<Champion, Pair<Integer, Integer>>> getPlayerEnemiesChampionsCoordsInVision(int playerId) {
        List<Pair<Champion, Pair<Integer, Integer>>> champions = new ArrayList<>();
        Square[][] squares = arena.getSquares();
        for (int i = 0; i < Globals.getArenaHeight(); i++) {
            for (int j = 0; j < Globals.getArenaWidth(); j++) {
                for (Champion champion : squares[i][j].getChampions()) {
                    if (playerId != champion.getPlayerId() && arena.getCurrentPlayerVision()[i][j]) {
                        champions.add(new Pair(champion, new Pair(i, j)));
                    }
                }
            }
        }
        return champions;
    }

    public void removeDeadChampions() {
        for (int i = 0; i < Globals.getArenaHeight(); i++) {
            for (int j = 0; j < Globals.getArenaWidth(); j++) {
                for (Champion champion : arena.getSquares()[i][j].getChampions()) {
                    if (champion.getStatus() == ChampionStatus.DEAD) {
                        arena.getSquares()[i][j].getChampions().remove(champion);

                        //respawn the items in the arena randomly.
                        for (Item item : champion.getItems()) {
                            spawnItem(item);
                            champion = buffManager.doDeBuffItem(champion, item);
                        }
                        champion.getItems().clear();
                    }
                }
            }
        }
    }

    public List<Item> collectItemsFromArena()
    {
        List<Item> items = new ArrayList<>();
        for (int i = 0; i < Globals.getArenaHeight(); i++) {
            for (int j = 0; j < Globals.getArenaWidth(); j++) {
                if(arena.getSquares()[i][j].getItemOnSquare() != null){
                    items.add(arena.getSquares()[i][j].getItemOnSquare());
                }
            }
        }
        return items;
    }

    /**
     * this class is responsible for the idea of performing an order (Basic Attack or Move).
     */
    public class OrderManager {

        ChampionAttributeManager championAttributeManager;

        OrderManager() {
            this.championAttributeManager = new ChampionAttributeManager();
        }


        /**
         * this functionality moves a champion from a specific place
         * on the arena to another.
         *
         * @param walkOrder has the direction, the amount of steps of moving and the champion id
         */

        public synchronized void moveMe(Walk walkOrder) {

            Champion champion = findChampionById(walkOrder.getChampionId());

            if (champion.isStunned()) {
                return;
            }
            //YOU CANT MOVE A DEAD CHAMPION
            if (champion.getStatus() == ChampionStatus.DEAD)
                return;

            //THE MOVEMENT SPEED IS ASSIGNED FROM THE ORDER DATA.
            int movementSpeed = walkOrder.getMovementSpeed();

            Pair<Integer, Integer> coords = findCoordinates(champion.getUniqueID());
            if (coords == null)
                return;

            int i = coords.getKey(), j = coords.getValue();
            boolean canTransfer = false;
            for (Champion ch : arena.getSquares()[i][j].getChampions()) {
                if (ch.getUniqueID() == champion.getUniqueID()) {
                    canTransfer = true;
                    break;
                }
            }

            for (int ii = 1; ii <= movementSpeed; ii++) {
                Champion remChamp = removeChampionByCoordinates(champion.getUniqueID(), i, j);

                try {

                    switch (walkOrder.getDirection()) {
                        case UP:
                            i--;
                            placeChampion(remChamp, i, j);
                            break;
                        case DOWN:
                            i++;
                            placeChampion(remChamp, i, j);
                            break;
                        case LEFT:
                            j--;
                            placeChampion(remChamp, i, j );
                            break;
                        case RIGHT:
                            j++;
                            placeChampion(remChamp, i, j);
                            break;
                    }

                    if(arena.getSquares()[i][j].getType() == TileType.WATER) {
                        ii++;
                    }

                } catch (InTerrainException e) {
                    undoMove(champion, walkOrder.getDirection(), i, j);
                    break;
                } catch(OutOfArenaException e) {

                    //puts the removed champion back to his last tile
                    try {
                        switch (walkOrder.getDirection()) {
                            case UP:
                                i++;
                                placeChampion(remChamp, i, j);
                                break;
                            case DOWN:
                                i--;
                                placeChampion(remChamp, i, j);
                                break;
                            case LEFT:
                                j++;
                                placeChampion(remChamp, i, j );
                                break;
                            case RIGHT:
                                j--;
                                placeChampion(remChamp, i, j);
                                break;
                        }
                    } catch(Exception ignored) {

                    }
                    break;
                }
            }

//            //MOVE IN THE DIRECTION WANTED WITH THE STEP YOU GOT (MOVEMENT SPEED)
//            switch (walkOrder.getDirection()) {
//                case UP:
//                    if (i - movementSpeed < Globals.getArenaHeight()
//                            && canTransfer) {
//                        Champion remChamp = removeChampionByCoordinates(champion.getUniqueID(), i, j);
//                        placeChampion(remChamp, i - movementSpeed, j);
//                    }
//                    break;
//                case DOWN:
//                    if (i + movementSpeed >= 0 && canTransfer) {
//                        Champion remChamp = removeChampionByCoordinates(champion.getUniqueID(), i, j);
//                        placeChampion(remChamp, i + movementSpeed, j);
//                    }
//                    break;
//                case LEFT:
//                    if (j - movementSpeed >= 0 && canTransfer) {
//                        Champion remChamp = removeChampionByCoordinates(champion.getUniqueID(), i, j);
//                        placeChampion(remChamp, i, j - movementSpeed);
//                    }
//                    break;
//                case RIGHT:
//                    if (j + movementSpeed < Globals.getArenaWidth()
//                            && canTransfer) {
//                        Champion remChamp = removeChampionByCoordinates(champion.getUniqueID(), i, j);
//                        placeChampion(remChamp, i, j + movementSpeed);
//                    }
//                    break;
//            }
        }

        private void undoMove(Champion champion, Direction direction, int i, int j) {
            Champion remChamp = removeChampionByCoordinates(champion.getUniqueID(), i, j);

            try {
                switch (direction) {
                    case UP:
                        placeChampion(remChamp, i + 1, j);
                        i++;
                    case DOWN:
                        placeChampion(remChamp, i - 1, j);
                        i--;
                    case LEFT:
                        placeChampion(remChamp, i, j + 1);
                        j++;
                    case RIGHT:
                        placeChampion(remChamp, i, j - 1);
                        j--;
                }
            } catch (Exception e) {
                //no need
            }


        }


        //**************************************************************************


        /**
         * this funtionality casts the order given to it and cast the basic attack
         *
         * @param attack oder that has the id of the attacker and the defender.
         */

        public synchronized void performAttack(Attack attack) {
            int attackerId = attack.getChampionId();
            int defenderId = attack.getChampionDestinationId();

            if (findChampionById(attackerId).isStunned()) {
                return;
            }

            //YOU CAN'T ATTACK IF YOU'RE NOT IN RANGE.
            if (inRange(attackerId, defenderId, false)) {
                this.championAttributeManager.basicAttack(findChampionById(attackerId),
                        findChampionById(defenderId));
            }
        }

        public synchronized void performAbility(AbilityOrder ability) {
            int championUserAbilityId = ability.getChampionId();
            Champion championUserAbility = findChampionById(championUserAbilityId);
            Ability championAbility = championUserAbility.getAbility();

            int championAttackedId = ability.getAttackedChampionId();

            List<Champion> enemyAffectedChampions = new ArrayList<>();
            List<Champion> alliesAffectedChampions = new ArrayList<>();

            //for checking if the attacked champion is not in range anymore
            if (championAttackedId != -1) {
                if (!inRange(championUserAbilityId, championAttackedId, false)) {
                    return;
                }
            }

            if (championAbility.getAoe() == 0) {
                if (championAbility.isInLine()) {
                    //fill the enemy list with champions in the same line
                } else if (championAbility.isBehindTheTarget()) {
                    enemyAffectedChampions.addAll(getBehindTheTargetChampions(championUserAbilityId, championAttackedId));
                }
            } else {
                enemyAffectedChampions.addAll(championsEnemyInAoe(championUserAbilityId));
                alliesAffectedChampions.addAll(championsAlliesInAoe(championUserAbilityId));
            }

            for (Champion champion : enemyAffectedChampions) {
                championAttributeManager.applyAbilityOnEnemy(championAbility, champion);
            }

            for (Champion champion : alliesAffectedChampions) {
                championAttributeManager.applyAbilityOnAlly(championAbility, champion);
            }

            championAttributeManager.applyAbilityOnSelf(championAbility, championUserAbility);
        }


        //**************************************************************************

    }

    private class ChampionAttributeManager {

        void basicAttack(Champion attacker, Champion defender) {
            int damage = attacker.getBasicAttackDamage();
            int criticalStrikeChance = attacker.getCriticalStrikeChance();
            int criticalStrikeDamage = attacker.getCriticalStrikeDamage();
            int armor = defender.getArmor();
            int health = defender.getCurrentHealth();

            if (attacker.getStatus() == ChampionStatus.DEAD || defender.getStatus() == ChampionStatus.DEAD)
                return;

            if (new Random().nextInt(100) < criticalStrikeChance)
                damage += (damage * criticalStrikeDamage) / 100;
            health -= (damage - ((damage * armor) / 100));

            defender.setCurrentHealth(health);

            attacker.setCurrentMana(attacker.getCurrentMana() + 1);

            //KILL THEM IF THEY DIDN'T DIE YET.
            if (health <= 0)
                defender.setStatus(ChampionStatus.DEAD);
        }

        public void applyAbilityOnEnemy(Ability championAbility, Champion champion) {
            //damage the enemy by the damage attribute
            applyDamage(championAbility.getDamage(), champion);

            //damage the enemy by the damage percent attribute
            applyDamage((champion.getHealth() * championAbility.getDamagePercent()) / 100, champion);

            //damage the enemy by the true damage attribute
            champion.setCurrentHealth(champion.getCurrentHealth() - championAbility.getTrueDamage());

            //reduce the same amount of enemy missing health by the damageAsMissingHealth attribute
            if (championAbility.isDamageAsMissingHealth()) {
                applyDamage(champion.getHealth() - champion.getCurrentHealth(), champion);
            }

            //checks if the champion is dead or not
            if (champion.getCurrentHealth() <= 0) {
                champion.setStatus(ChampionStatus.DEAD);
            }

            //set the stunning period for a champion by the stun attribute
            int stun = champion.getStunnedFor();
            stun += championAbility.getStun();
            champion.setStunnedFor(stun);

            //reduce the armor by the armorReduce Attribute
            champion.setArmor(champion.getArmor() - championAbility.getArmorReduce());

            //reduce the magic resist by the magicResist Attribute
            champion.setMagicResist(champion.getMagicResist() - championAbility.getArmorReduce());

            //reduce the basic attack amount by the basicAttackReduce attribute
            champion.setBasicAttackDamage(champion.getBasicAttackDamage() - championAbility.getBasicAttackReduce());

            //steals the mana from the enemy and adds it to the user of the ability by the manaSteel attribute
            champion.setCurrentMana(champion.getCurrentMana() - championAbility.getManaSteal());
            Champion abilityUserChampion = findChampionById(championAbility.getChampionId());
            abilityUserChampion.setCurrentMana(abilityUserChampion.getCurrentMana() - championAbility.getManaSteal());

            //burns the mana of the enemy by the manaBurn attribute
            if (championAbility.isManaBurn()) {
                champion.setCurrentMana(0);
            }
        }

        void applyAbilityOnAlly(Ability ability, Champion champion) {
            //heal the champion by the allyHeal attribute
            champion.setCurrentHealth(champion.getCurrentHealth() + ability.getAllyHeal());

            //buff the range of the champion by buffRange attribute
            champion.setAttackRange(champion.getAttackRange() + ability.getBuffRange());

            //buff the basic attack damage by buffDamage attribute
            champion.setBasicAttackDamage(champion.getBasicAttackDamage() + ability.getBuffDamage());

            //buff the current mana by buffMana attribute
            champion.setCurrentMana(champion.getCurrentMana() + ability.getBuffMana());
        }

        void applyDamage(int damageAmount, Champion champion) {
            int armor = champion.getArmor();
            int health = champion.getCurrentHealth();

            if (champion.getStatus() == ChampionStatus.DEAD) {
                return;
            }

            health -= (damageAmount - ((damageAmount * armor) / 100));
            champion.setCurrentHealth(health);

            //KILL THEM IF THEY DIDN'T DIE YET.
            if (health <= 0) {
                champion.setStatus(ChampionStatus.DEAD);
            }
        }

        public void applyAbilityOnSelf(Ability ability, Champion championUserAbility) {
            championUserAbility.setCurrentHealth(championUserAbility.getCurrentHealth() + ability.getSelfHeal());
        }
    }


    /**
     * construct a new Arena based on Globals..
     */
    public BattleArenaManager() {
        this.arena = new BattleArena(Globals.getArenaHeight(), Globals.getArenaWidth());
        initializeTilesForArena();
        this.orderManager = new OrderManager();
        this.buffManager = new BuffManager();
    }

    private void initializeTilesForArena() {
        for (int i = 0; i < Globals.getArenaWidth(); i++) {
            for (int j = 0; j < Globals.getArenaHeight(); j++) {
                this.arena.getSquares()[i][j].setType(TileType.STANDARD);
            }
        }
    }

    //**************************************************************************

    /**
     * pass the champion id (Unique number)
     * and look up in the Arena for its coordinates.
     *
     * @param championId Unique id for every champion.
     * @return Pair of coordinates.
     */
    public Pair<Integer, Integer> findCoordinates(int championId) {
        for (int i = 0; i < Globals.getArenaWidth(); i++) {
            for (int j = 0; j < Globals.getArenaHeight(); j++) {
                for (Champion champion : arena.getSquares()[i][j].getChampions()) {
                    if (championId == champion.getUniqueID())
                        return new Pair(i, j);
                }
            }
        }
        return null;
    }


    //**************************************************************************


    /**
     * since only the orders are being executed on the arena manager
     * battleArenaManager is responsible for executing the orders, this is why this method exists here.
     *
     * @param orders of the current player.
     */

    public void startExecutionPhase(List<Order> orders) {

        //for each order
        //cast the appropriate method
        /**
         *
         * switch (order)
         *
         *  attack => performAttack()
         *  walk = > walkMe()
         */

        ExecutorService executor = Executors.newFixedThreadPool(10);
        for (Order order : orders) {
            Runnable runnableOrderTask = ()-> {
                if(order instanceof Walk) {
                    orderManager.moveMe((Walk)order);
                } else if(order instanceof Attack) {
                    orderManager.performAttack((Attack) order);
                } else if(order instanceof AbilityOrder) {
                    orderManager.performAbility((AbilityOrder) order);
                }
            };
            executor.execute(runnableOrderTask);
        }
        executor.shutdown();
        try {
            executor.awaitTermination(100, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    //**************************************************************************


    /**
     * this funtionality returns the Object of the champion you're looking for
     *
     * @param championId the UniqueId of the champion
     * @return the champion
     */

    public Champion findChampionById(int championId) {
        for (int i = 0; i < Globals.getArenaWidth(); i++) {
            for (int j = 0; j < Globals.getArenaHeight(); j++) {
                for (Champion ch : arena.getSquares()[i][j].getChampions()) {
                    if (ch.getUniqueID() == championId) {
                        return ch;
                    }
                }
            }
        }
        return null;
    }
    //**************************************************************************

    /**
     * this functionality places the champion in the specific i & j positions.
     * if and only if this placement is allowed &&&& the champions on the arena are
     * still less than the maximum number.
     *
     * @param passedChampion the champion you want to place
     * @param i              y coordinate
     * @param j              x coordinate
     * @return true or false
     */

    public boolean placeChampion(Champion passedChampion, int i, int j) throws OutOfArenaException, InTerrainException {
        try {

            //checks if the square you are placing your champion on is not a terrain.
            if (arena.getSquares()[i][j].getType() == TileType.TERRAIN)
                throw new InTerrainException();


            if (playerChampionsCoords(passedChampion.getPlayerId()).size() < Globals.getNumberOfChampionsOnArena()
                    && isPlacementAllowedForPlayer(passedChampion.getPlayerId(), i, j)) {
                passedChampion.setPlace(ChampionPlace.BATTLE_ARENA);
                arena.getSquares()[i][j].getChampions().add(passedChampion);


                if(arena.getSquares()[i][j].getItemOnSquare() != null) {
                    Item item = arena.getSquares()[i][j].getItemOnSquare();
                    if (passedChampion.getItems().size() < Globals.getMaxItemsOnChampion()) {
                        passedChampion.getItems().add(getArena().getSquares()[i][j].getItemOnSquare());
                        getArena().getSquares()[i][j].setItemOnSquare(null);
                        passedChampion = buffManager.doBuffItem(passedChampion, item);
                        System.out.println("item was added successfully");
                    }
                }

                return true;
            }
        } catch (IndexOutOfBoundsException e) {
            throw new OutOfArenaException();
        }

        try {
            throw new IndexOutOfBoundsException();
        } catch(IndexOutOfBoundsException e) {
            throw new OutOfArenaException();
        }
        //return false;
    }

    private void removeItemOnCoords(int i, int j) {
        arena.getSquares()[i][j].setItemOnSquare(null);
    }

    //**************************************************************************

    /**
     * the placement is not allowed for the player if he already has a champion
     * in the same square.
     *
     * @param playerId the id of the player is making the placement.
     * @param i
     * @param j
     * @return
     */

    public boolean isPlacementAllowedForPlayer(int playerId, int i, int j) {

        if (i > Globals.getArenaHeight() || i < 0 && j > Globals.getArenaWidth() || j < 0)
            return false;

        for (Champion champion : arena.getSquares()[i][j].getChampions()) {
            if (champion.getPlayerId() == playerId) {
                return false;
            }
        }
        return true;
    }

    //**************************************************************************

    /**
     * for checking if the maxium number of champion has been reached
     */

    public boolean isFull(int playerId) {
        if (this.playerChampionsCoords(playerId).size() == Globals.getMaxTeamSize())
            return true;
        return false;
    }

    //**************************************************************************

    /**
     * for checking if a player can add a champion in a square of coords i and j.
     *
     * @param playerId
     * @param i
     * @param j
     * @return
     */

    public boolean isSquareValid(int playerId, int i, int j) {
        i--;
        j--;
        for (Champion ch : this.arena.getSquares()[i][j].getChampions()) {
            if (ch.getPlayerId() == playerId)
                return true;
        }
        return false;
    }

    //**************************************************************************

    /**
     * this functionality collects the champions and their i and j positions
     * for a specific player.
     *
     * @param playerId the player to gather his champions and their (i&j)s
     * @return list of champions and their positions.
     */

    public List<Pair<Champion, Pair<Integer, Integer>>> playerChampionsCoords(int playerId) {
        List<Pair<Champion, Pair<Integer, Integer>>> championsCoords = new ArrayList<>();
        Square[][] squares = arena.getSquares();
        for (int i = 0; i < Globals.getArenaHeight(); i++) {
            for (int j = 0; j < Globals.getArenaWidth(); j++) {
                for (Champion champion : squares[i][j].getChampions()) {
                    if (playerId == champion.getPlayerId()) {
                        championsCoords.add(new Pair(champion, new Pair(i, j)));
                    }
                }
            }
        }
        return championsCoords;
    }

    //**************************************************************************

    /**
     * this functionality collects the champions for a specific player.
     *
     * @param playerId the player to gather his champions and their (i&j)s
     * @return list of champions and their positions.
     */

    public List<Champion> getBattleArenaChampions(int playerId) {
        List<Champion> champions = new ArrayList<>();
        Square[][] squares = arena.getSquares();
        for (int i = 0; i < Globals.getArenaHeight(); i++) {
            for (int j = 0; j < Globals.getArenaWidth(); j++) {
                for (Champion champion : squares[i][j].getChampions()) {
                    if (playerId == champion.getPlayerId()) {
                        champions.add(champion);
                    }
                }
            }
        }
        return champions;
    }


    //**************************************************************************

    /**
     * removes a champion from the arena based on their id.
     *
     * @param championId
     * @param i
     * @param j
     * @return
     */

    public Champion removeChampionByCoordinates(int championId, int i, int j) {
        Champion championReturn = null;

        for (Champion champion : arena.getSquares()[i][j].getChampions()) {
            if (champion.getUniqueID() == championId) {
                championReturn = champion;
                arena.getSquares()[i][j].getChampions().remove(champion);
                return championReturn;
            }
        }

        return championReturn;
    }

    //**************************************************************************

    /**
     * this function could be used for both ranges (vision range and attack range)
     * and we want to check if the player with id2 is in range of player with id1.
     *
     * @param championId1
     * @param championId2
     * @param vision
     * @return
     */


    public boolean inRange(int championId1, int championId2, boolean vision) {
        Pair<Integer, Integer> champion1Coordinates = findCoordinates(championId1);
        Pair<Integer, Integer> champion2Coordinates = findCoordinates(championId2);
        int range;
        if (vision) //=> the range is for vision range
            range = findChampionById(championId1).getVisionRange();
        else       //=> the range is for attack range
            range = findChampionById(championId1).getAttackRange();

        if (champion1Coordinates == null || champion2Coordinates == null) return false;

        return (Math.abs(champion1Coordinates.getKey() - champion2Coordinates.getKey()) <= range &&
                Math.abs(champion1Coordinates.getValue() - champion2Coordinates.getValue()) <= range);
    }


    //**************************************************************************

    /**
     * updates the player vision matrix with false values for the next scan
     */

    private void reinitializePlayerVision() {
        for (int i = 0; i < Globals.getArenaHeight(); i++) {
            for (int j = 0; j < Globals.getArenaWidth(); j++) {
                arena.getCurrentPlayerVision()[i][j] = false;
            }
        }
    }

    //**************************************************************************

    /**
     * updates the vision of the player of the current arena.
     *
     * @param playerId
     */


    public void updateVision(int playerId) {
        Square[][] squares = arena.getSquares();
        reinitializePlayerVision();
        for (int i = 0; i < Globals.getArenaHeight(); i++) {
            for (int j = 0; j < Globals.getArenaWidth(); j++) {
                for (Champion champion : squares[i][j].getChampions()) {
                    if (playerId == champion.getPlayerId()) {
                        int championVisionRange = champion.getVisionRange();
                        boolean championInBush = false;
                        if (squares[i][j].getType() == TileType.GRASS)
                            championInBush = true;


                        for (int ii = 0; ii < Globals.getArenaHeight(); ii++) {
                            for (int jj = 0; jj < Globals.getArenaHeight(); jj++) {

                                /*
                                if a player champion is in a bush, he can see everyone's champions
                                if not in a bush he can see everyone outside bushes
                                 */

                                if ((championInBush || squares[ii][jj].getType() != TileType.GRASS) &&
                                        ii >= i - championVisionRange && ii <= i + championVisionRange
                                        && jj >= j - championVisionRange && jj <= j + championVisionRange) {
                                    arena.getCurrentPlayerVision()[ii][jj] = true;
                                }
                            }
                        }

                    }
                }
            }
        }
    }

    //**************************************************************************


    /**
     * @param championUserAbilityId Champion how has ability
     * @return List Of Defender champion
     */


    public List<Champion> championsEnemyInAoe(int championUserAbilityId) {
        List<Champion> enemyChampions = new ArrayList<>();
        Pair<Integer, Integer> championUserAbilityCoords = findCoordinates(championUserAbilityId);
        Champion championUserAbility = findChampionById(championUserAbilityId);
        int startX = Math.max(championUserAbilityCoords.getKey() - championUserAbility.getAbility().getAoe(), 0);
        int endX = Math.min(championUserAbilityCoords.getKey() + championUserAbility.getAbility().getAoe(), Globals.getArenaWidth() - 1);
        int startY = Math.max(championUserAbilityCoords.getValue() - championUserAbility.getAbility().getAoe(), 0);
        int endY = Math.min(championUserAbilityCoords.getValue() + championUserAbility.getAbility().getAoe(), Globals.getArenaHeight() - 1);
        for (int i = startX; i <= endX; i++) {
            for (int j = startY; j <= endY; j++) {
                for (Champion champion : arena.getSquares()[i][j].getChampions()) {
                    if (champion.getPlayerId() != championUserAbility.getPlayerId())
                        enemyChampions.add(champion);
                }
            }
        }
        return enemyChampions;
    }

    /**
     * @param championUserAbilityId
     * @return List of friends Champion To buff them By ability
     */
    public List<Champion> championsAlliesInAoe(int championUserAbilityId) {
        List<Champion> alliesChampions = new ArrayList<>();
        Pair<Integer, Integer> championUserAbilityCoords = findCoordinates(championUserAbilityId);
        Champion championUserAbility = findChampionById(championUserAbilityId);
        int startX = Math.max(championUserAbilityCoords.getKey() - championUserAbility.getAbility().getAoe(), 0);
        int endX = Math.min(championUserAbilityCoords.getKey() + championUserAbility.getAbility().getAoe(), Globals.getArenaWidth() - 1);
        int startY = Math.max(championUserAbilityCoords.getValue() - championUserAbility.getAbility().getAoe(), 0);
        int endY = Math.min(championUserAbilityCoords.getValue() + championUserAbility.getAbility().getAoe(), Globals.getArenaHeight() - 1);
        for (int i = startX; i <= endX; i++) {
            for (int j = startY; j <= endY; j++) {
                for (Champion champion : arena.getSquares()[i][j].getChampions()) {
                    if (champion.getPlayerId() == championUserAbility.getPlayerId())
                        alliesChampions.add(champion);
                }
            }
        }
        return alliesChampions;
    }

    //**************************************************************************

    /**
     * @param championId1 Attack Champion
     * @param championId2 Defender Champion
     * @return List Of champion Behind Th Target Champion
     */
    public List<Champion> getBehindTheTargetChampions(int championId1, int championId2) {
        List<Champion> returnList = new ArrayList<>();
        Pair<Integer, Integer> champion1Coordinates = findCoordinates(championId1);
        Pair<Integer, Integer> champion2Coordinates = findCoordinates(championId2);
        Champion playerChampion = findChampionById(championId1);
        int dx = champion2Coordinates.getKey() - champion1Coordinates.getKey();
        int dy = champion2Coordinates.getValue() - champion1Coordinates.getValue();
        if (dx == 0 && dy == 0) return returnList;
        if (dx != 0 && dy != 0) {
            int x = (dx > 0) ? 1 : -1;
            int y = (dy > 0) ? 1 : -1;
            for (int i = champion2Coordinates.getKey(), j = champion2Coordinates.getValue();
                 i != Globals.getArenaWidth() && i != -1 && j != Globals.getArenaHeight() && j != -1; i += x, j += y) {
                for (Champion champion : arena.getSquares()[i][j].getChampions()) {
                    if (champion.getPlayerId() != playerChampion.getPlayerId())
                        returnList.add(champion);
                }
            }
        } else if (dy == 0) {
            int x = (dx > 0) ? 1 : -1;
            for (int i = champion2Coordinates.getKey(); i != Globals.getArenaWidth() && i != -1; i += x) {
                for (Champion champion : arena.getSquares()[i][champion2Coordinates.getValue()].getChampions()) {
                    if (champion.getPlayerId() != playerChampion.getPlayerId())
                        returnList.add(champion);
                }
            }
        } else {
            int y = (dy > 0) ? 1 : -1;
            for (int i = champion2Coordinates.getValue(); i != Globals.getArenaHeight() && i != -1; i += y) {
                for (Champion champion : arena.getSquares()[champion2Coordinates.getKey()][i].getChampions()) {
                    if (champion.getPlayerId() != playerChampion.getPlayerId())
                        returnList.add(champion);
                }
            }
        }
        return returnList;
    }
    //**************************************************************************


    /**
     * this method helps in spawning an item on the map;
     *
     * @param item
     */

    public void spawnItem(Item item) {
        boolean added = false;
        while (!added) {
            int i = new Random().nextInt(Globals.getArenaWidth());
            int j = new Random().nextInt(Globals.getArenaHeight());
            if (this.getArena().getSquares()[i][j].getItemOnSquare() == null
                    && this.getArena().getSquares()[i][j].getType()!=TileType.TERRAIN) {
                this.getArena().getSquares()[i][j].setItemOnSquare(item);
                added = true;
            }
        }
    }


    /**
     * this method makes a random item object..
     *
     * @return
     */

    public Item randomItemSpawnSelect(int id) {
        Item randomItem = null;
        ItemsNames[] names = ItemsNames.values();
        int randomSelect = new Random().nextInt(names.length);
        ItemsNames chosenItemName = names[randomSelect];

        switch (chosenItemName) {
//            case MAGIC_HAT:
//                randomItem = new MagicHat(id);
//                break;
            case ANGRY_CLOAK:
                randomItem = new AngryCloak(id);
                break;
            case NIGHT_SHIFT:
                randomItem = new NightShift(id);
                break;
            case KNIGHT_ARMOR:
                randomItem = new KnightArmor(id);
                break;
            case WARIOR_GLOVE:
                randomItem = new WarirorGlove(id);
                break;
            case UNIVERSE_CORE:
                randomItem = new UniverseCore(id);
                break;
            case VOID_HIT:
                randomItem = new VoidHit(id);
                break;
        }
        return randomItem;
    }


    /**
     * getters and setters ...
     */

    public Item getItemInASquare(int i, int j)
    {
        return this.arena.getSquares()[i][j].getItemOnSquare();
    }

    public BattleArena getArena() {
        return arena;
    }

    public void setArena(BattleArena arena) {
        this.arena = arena;
    }

    public BuffManager getBuffManager() {
        return buffManager;
    }

    public void setBuffManager(BuffManager buffManager) {
        this.buffManager = buffManager;
    }
}
