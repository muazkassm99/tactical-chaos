package sample.Model;

public class Ability {
    private int championId;

    private int aoe; //apply on champions in an area
    private boolean inLine; // apply on champions in a line
    private boolean behindTheTarget; //do for all behind the target

    private int damage; //damage the enemy
    private int damagePercent; //damage the enemy by a percent of his max health
    private int trueDamage;
    private int stun; //stuns for amount of rounds
    private int armorReduce; //reduces enemy armor
    private int magicResistReduce; //reduces enemy magic resist
    private int basicAttackReduce; //reduces enemy basic attack
    private boolean damageAsMissingHealth; //deals same amount of enemy missing health
    private int manaSteal; //does a mana steal when basic attack is performed and when it is activated
    private boolean manaBurn; //burns enemy mana and make it 0 for 1 round

    private int selfHeal; //heal itself for
    private int allyHeal; //heal its ally for
    private int buffRange;
    private int buffDamage;
    private int buffMana;

    Ability() {

    }

    public Ability(int championId) {
        this.championId = championId;
        aoe = 0;
        inLine = false;
        behindTheTarget = false;

        damage = 0;
        damagePercent = 0;
        trueDamage = 0;
        stun = 0;
        armorReduce = 0;
        magicResistReduce = 0;
        basicAttackReduce = 0;
        damageAsMissingHealth = false;
        manaSteal = 0;
        manaBurn = false;

        selfHeal = 0;
        allyHeal = 0;
        buffRange = 0;
        buffDamage = 0;
        buffMana = 0;
    }

    public Ability(int championId, int aoe, boolean inLine, boolean behindTheTarget, int damage, int damagePercent, int trueDamage, int stun, int armorReduce, int magicResistReduce, int basicAttackReduce, boolean damageAsMissingHealth, boolean manaBurn, int selfHeal, int allyHeal, int buffRange, int buffDamage, int buffMana) {
        this.championId = championId;
        this.aoe = aoe;
        this.inLine = inLine;
        this.behindTheTarget = behindTheTarget;
        this.damage = damage;
        this.damagePercent = damagePercent;
        this.trueDamage = trueDamage;
        this.stun = stun;
        this.armorReduce = armorReduce;
        this.magicResistReduce = magicResistReduce;
        this.basicAttackReduce = basicAttackReduce;
        this.damageAsMissingHealth = damageAsMissingHealth;
        this.manaBurn = manaBurn;
        this.selfHeal = selfHeal;
        this.allyHeal = allyHeal;
        this.buffRange = buffRange;
        this.buffDamage = buffDamage;
        this.buffMana = buffMana;
    }

    public int getChampionId() {
        return championId;
    }

    public void setChampionId(int championId) {
        this.championId = championId;
    }

    public int getAoe() {
        return aoe;
    }

    public void setAoe(int aoe) {
        this.aoe = aoe;
    }

    public boolean isInLine() {
        return inLine;
    }

    public void setInLine(boolean inLine) {
        this.inLine = inLine;
    }

    public boolean isBehindTheTarget() {
        return behindTheTarget;
    }

    public void setBehindTheTarget(boolean behindTheTarget) {
        this.behindTheTarget = behindTheTarget;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getDamagePercent() {
        return damagePercent;
    }

    public void setDamagePercent(int damagePercent) {
        this.damagePercent = damagePercent;
    }

    public int getTrueDamage() {
        return trueDamage;
    }

    public void setTrueDamage(int trueDamage) {
        this.trueDamage = trueDamage;
    }

    public int getStun() {
        return stun;
    }

    public void setStun(int stun) {
        this.stun = stun;
    }

    public int getArmorReduce() {
        return armorReduce;
    }

    public void setArmorReduce(int armorReduce) {
        this.armorReduce = armorReduce;
    }

    public int getMagicResistReduce() {
        return magicResistReduce;
    }

    public void setMagicResistReduce(int magicResistReduce) {
        this.magicResistReduce = magicResistReduce;
    }

    public int getBasicAttackReduce() {
        return basicAttackReduce;
    }

    public void setBasicAttackReduce(int basicAttackReduce) {
        this.basicAttackReduce = basicAttackReduce;
    }

    public boolean isDamageAsMissingHealth() {
        return damageAsMissingHealth;
    }

    public void setDamageAsMissingHealth(boolean damageAsMissingHealth) {
        this.damageAsMissingHealth = damageAsMissingHealth;
    }

    public int getManaSteal() {
        return manaSteal;
    }

    public void setManaSteal(int manaSteal) {
        this.manaSteal = manaSteal;
    }

    public boolean isManaBurn() {
        return manaBurn;
    }

    public void setManaBurn(boolean manaBurn) {
        this.manaBurn = manaBurn;
    }

    public int getSelfHeal() {
        return selfHeal;
    }

    public void setSelfHeal(int selfHeal) {
        this.selfHeal = selfHeal;
    }

    public int getAllyHeal() {
        return allyHeal;
    }

    public void setAllyHeal(int allyHeal) {
        this.allyHeal = allyHeal;
    }

    public int getBuffRange() {
        return buffRange;
    }

    public void setBuffRange(int buffRange) {
        this.buffRange = buffRange;
    }

    public int getBuffDamage() {
        return buffDamage;
    }

    public void setBuffDamage(int buffDamage) {
        this.buffDamage = buffDamage;
    }

    public int getBuffMana() {
        return buffMana;
    }

    public void setBuffMana(int buffMana) {
        this.buffMana = buffMana;
    }
}
