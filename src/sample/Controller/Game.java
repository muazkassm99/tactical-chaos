package sample.Controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;
import sample.Model.Enums.ChampionClass;
import sample.Model.Itmes.*;
import sample.Model.OrderAttributes.*;
import sample.Utilities.Globals;
import sample.View.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Game implements SaveGameCallBack {
    private MasterManager masterManager;

    public Game() {
        initializeGame();
        this.masterManager = new MasterManager(this);
    }

    private void initializeGame() {
        while (true) {
            System.out.println("There is a saved game, would you like to load the game?");
            String choice = Globals.getScanner().next();
            if (choice.equalsIgnoreCase("yes")) {
                loadGame();
                masterManager.setSaveGameCallBack(this);
                continueGame();
                return;
            } else if (choice.equalsIgnoreCase("no")) {
                break;
            } else {
                System.out.println("invalid input, re enter your choice");
            }
        }

        int widthArenaSize, heightArenaSize, maxTeamSize, numberOfRounds, maxAllowedSwaps; boolean recordGame;
        List<ChampionClass> acceptedChampionClasses = new ArrayList<>();
        System.out.print("Enter width size:");
        widthArenaSize = Globals.getScanner().nextInt();
        System.out.print("Enter height size:");
        heightArenaSize = Globals.getScanner().nextInt();
        System.out.print("Enter max team size:");
        maxTeamSize = Globals.getScanner().nextInt();
        System.out.print("Enter number of rounds (minimum: 10):");
        numberOfRounds = Globals.getScanner().nextInt();
        System.out.print("Enter maximum number of allowed swaps:");
        maxAllowedSwaps = Globals.getScanner().nextInt();
        System.out.println("Would you like to record the game?");
        String choice = Globals.getScanner().next();
        recordGame = choice.equalsIgnoreCase("yes");

        int numberOfAcceptedClasses;
        System.out.print("Enter number of Accepted classes in game (zero for accepting the default number of champion classes):");
        while (true) {
            try {
                numberOfAcceptedClasses = Globals.getScanner().nextInt();
                if (numberOfAcceptedClasses < 0 || numberOfAcceptedClasses >= ChampionClass.values().length) {
                    System.out.println("Enter a number larger than a zero and lower than the number of champion classes ("
                            + ChampionClass.values().length + ")");
                }
                break;
            } catch (Exception e) {
                System.out.println("Please enter a valid number between 0 and lower than the number of champion classes ("
                        + ChampionClass.values().length + ")");
            }
        }

        if (numberOfAcceptedClasses == 0) {
            for (ChampionClass championClass : ChampionClass.values()) {
                Globals.getGameChampionClasses().add(championClass);
            }
            System.out.println("All champion classes has been added to the game.");
            System.out.println("Added classes:");
            for (ChampionClass championClass : Globals.getGameChampionClasses()) {
                System.out.println(championClass.toString());
            }
        } else {
            int counter = 1;
            for (ChampionClass c : ChampionClass.values()) {
                System.out.print(c.toString() + " " + (counter++) + " | ");
            }
            ;
            System.out.println();

            while (numberOfAcceptedClasses-- != 0) {
                int acceptedClass = Globals.getScanner().nextInt();

                boolean check = false;
                for (ChampionClass championClass : acceptedChampionClasses) {
                    if (championClass == ChampionClass.values()[acceptedClass - 1]) {
                        check = true;
                    }
                }

                if (check) {
                    System.out.println("This class has already been chosen.");
                    numberOfAcceptedClasses++;
                } else {
                    acceptedChampionClasses.add(ChampionClass.values()[acceptedClass - 1]);
                    System.out.println(ChampionClass.values()[acceptedClass - 1].toString() + " has been added to the game!");
                }
            }
        }

        System.out.println("Added classes:");
        for (ChampionClass championClass : acceptedChampionClasses) {
            System.out.println(championClass.toString());
        }

        Globals.setArenaWidth(widthArenaSize);
        Globals.setArenaHeight(heightArenaSize);
        Globals.setMaxTeamSize(maxTeamSize);
        Globals.setNumberOfRounds(numberOfRounds);
        Globals.setMaxNumberOfSwaps(maxAllowedSwaps);
        Globals.setGameChampionClasses(acceptedChampionClasses);
        Globals.setRecordGame(recordGame);
    }

    public void startGame() {
        masterManager.executeRounds();
    }

    private boolean loadGame() {
        StringBuilder deSerializedGameBuilder = new StringBuilder();
        File file = new File("SavedGame.json");
        try {
            FileReader reader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;
            while((line = bufferedReader.readLine()) != null) {
                deSerializedGameBuilder.append(line).append('\n');
            }
        } catch(IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
        }
        String deSerializedGame = null;
        if(deSerializedGameBuilder != null) {
            deSerializedGame = deSerializedGameBuilder.toString();
        }

        Gson gson = getMagicGson();
        MasterManager masterManager = gson.fromJson(deSerializedGame, MasterManager.class);
        this.masterManager = masterManager;
        masterManager.loadGameVariables();

        return true;
    }

    private Gson getMagicGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();

        RuntimeTypeAdapterFactory<Player> playerAdapter = RuntimeTypeAdapterFactory
                .of(Player.class, "playerType")
                .registerSubtype(ConsolePlayer.class, PlayerType.CONSOLE.name())
                .registerSubtype(GuiPLayer.class, PlayerType.GUI.name())
                .registerSubtype(BotPlayer.class, PlayerType.BOT.name());

        gsonBuilder.registerTypeAdapterFactory(playerAdapter);

        RuntimeTypeAdapterFactory<Item> itemAdapter = RuntimeTypeAdapterFactory
                .of(Item.class, "itemType")
                .registerSubtype(AngryCloak.class, ItemsClassNames.ANGRYCLOAK.name())
                .registerSubtype(KnightArmor.class, ItemsClassNames.KNIGHTARMOR.name())
                .registerSubtype(MagicHat.class, ItemsClassNames.MAGICHAT.name())
                .registerSubtype(NightShift.class, ItemsClassNames.NIGHTSHIFT.name())
                .registerSubtype(UniverseCore.class, ItemsClassNames.UNIVERSECORE.name())
                .registerSubtype(VoidHit.class, ItemsClassNames.VOIDHIT.name())
                .registerSubtype(WarirorGlove.class, ItemsClassNames.WARIORGLOVE.name());

        gsonBuilder.registerTypeAdapterFactory(itemAdapter);

        RuntimeTypeAdapterFactory<Order> orderAdapter = RuntimeTypeAdapterFactory
                .of(Order.class, "orderType")
                .registerSubtype(Walk.class, OrderClassNames.WALK.name())
                .registerSubtype(Attack.class, OrderClassNames.ATTACK.name())
                .registerSubtype(AbilityOrder.class, OrderClassNames.ABILITYORDER.name());

        gsonBuilder.registerTypeAdapterFactory(orderAdapter);

        return gsonBuilder.create();
    }

    public void continueGame() {
        masterManager.executeRounds();
    }


    @Override
    public void onSaveGame() {
        masterManager.saveGlobalVariables();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        String serializedGame = gson.toJson(masterManager);
        File savedGame = new File("SavedGame.json");
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(savedGame));
            writer.write(serializedGame);
            writer.flush();
           /* FileWriter writer = new FileWriter(savedGame);
            writer.write(serializedGame);*/
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
