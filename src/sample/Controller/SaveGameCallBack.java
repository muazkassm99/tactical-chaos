package sample.Controller;

public interface SaveGameCallBack {
    void onSaveGame();
}
