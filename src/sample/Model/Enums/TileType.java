package sample.Model.Enums;

public enum TileType {
    STANDARD, GRASS, TERRAIN, WATER
}
