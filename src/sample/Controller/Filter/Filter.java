package sample.Controller.Filter;

abstract public class Filter {
    public abstract boolean isAccepted(Object object);
}
