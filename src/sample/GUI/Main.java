package sample.GUI;

import com.sun.jdi.event.StepEvent;
import javafx.application.Application;
import javafx.beans.binding.ObjectBinding;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import sample.GUI.HomeScreen;
import sample.Model.Enums.ChampionClass;
import sample.View.GuiPLayer;

import javax.swing.tree.FixedHeightLayoutCache;
import java.util.*;

public class Main extends Application {

    Button playNowBtn, settingsBtn;
    Scene welcomeScene, settingsScene;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        //HomeScreen.display();

        HomeScreen.display();
        //todo GuiPLayer guiPLayer = new GuiPLayer(1);
//        StartGame game = new StartGame(primaryStage, Settings.display());
//        game.start();


    }


    void startGame(Stage primaryStage, Map<String, Object> settingsMap)
    {


    }



    void play()
    {
        Stage primaryStage = new Stage();
        Map<String, Object> settingsMap = new HashMap<>();
        Button playNow = new Button("Play Now");
        Button settings = new Button("Settings");

        playNow.setScaleX(1.5);
        playNow.setScaleY(1.5);
        settings.setScaleX(1.5);
        settings.setScaleY(1.5);

        VBox layout = new VBox(30);

        layout.getChildren().addAll(playNow, settings);
        layout.setAlignment(Pos.CENTER);
        Scene startScene = new Scene(layout, 600, 400);

        settings.setOnAction(e-> {

            settingsMap.putAll( Settings.display() );

            System.out.println(settingsMap.get("height").toString());
            System.out.println(settingsMap.get("width").toString());
            System.out.println(settingsMap.get("maxSwaps").toString());
            System.out.println(settingsMap.get("maxTeam").toString());
            System.out.println(settingsMap.get("maxRounds").toString());
            for (ChampionClass c:
                    (List<ChampionClass>)settingsMap.get("acceptedClasses")) {
                System.out.println(c);
            }

        });

        playNow.setOnAction(e->
        {
            startGame(primaryStage, settingsMap);
        });

        primaryStage.setScene(startScene);
        primaryStage.show();
    }




}
