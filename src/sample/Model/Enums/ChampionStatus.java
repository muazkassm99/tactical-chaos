package sample.Model.Enums;

public enum ChampionStatus {
    DEAD, DAMAGED, ALIVE
}
