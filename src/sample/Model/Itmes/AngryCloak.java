package sample.Model.Itmes;

import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;

public class AngryCloak extends Item {

    AngryCloak() {

    }

    public AngryCloak(int id)
    {
        super(ItemsClassNames.ANGRYCLOAK.name(), id);

        this.setCriticalStrikeIncrease(10);

        this.setChampionClass(ChampionClass.YORDLE);
    }

    @Override
    public String toString() {
        return "Angry Cloak";
    }
}
