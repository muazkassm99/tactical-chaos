package sample.GUI.Dialogs;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.util.concurrent.atomic.AtomicReference;

public class CoordsSelector {
    public static Pair<Integer, Integer> select()
    {
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("coords");
        AtomicReference<Pair<Integer, Integer>> ret = new AtomicReference<>();

        TextField i = new TextField("i");
        TextField j = new TextField("j");
        Button confrim = new Button("ok");

        window.setOnCloseRequest(e->
                window.close()
        );
        confrim.setOnAction(e->
        {
            ret.set(new Pair(Integer.parseInt(i.getText()), Integer.parseInt(j.getText())));
            window.close();
        });

        VBox vBox = new VBox();
        HBox hBox = new HBox();
        hBox.getChildren().addAll(i, j);
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(10);
        vBox.getChildren().addAll(hBox, confrim);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);

        Scene scene = new Scene(vBox, 200, 100);
        window.setScene(scene);
        window.showAndWait();

        return ret.get();

    }
}
