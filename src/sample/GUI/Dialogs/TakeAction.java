package sample.GUI.Dialogs;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.concurrent.atomic.AtomicInteger;

public class TakeAction {
    public static int takeActionBench()
    {
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("action");
        AtomicInteger ret = new AtomicInteger();

        Button sellBtn = new Button("Sell");
        Button toArena = new Button("To Arena");
        Button swapBtn = new Button("Swap");

        sellBtn.setOnAction(e->
        {
            ret.set(1);
            window.close();

        });

        toArena.setOnAction(e->
        {

            ret.set(2);
            window.close();

        });

        swapBtn.setOnAction(e->
        {
            ret.set(3);
            window.close();


        });

        VBox vBox = new VBox();
        vBox.getChildren().addAll(sellBtn, toArena, swapBtn);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);

        Scene scene = new Scene(vBox, 200, 100);
        window.setScene(scene);
        window.showAndWait();

        return ret.get();

    }
    public static int takeActionArena()
    {
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("action");
        AtomicInteger ret = new AtomicInteger();

        Button sellBtn = new Button("Sell");
        Button toBench = new Button("To Bench");
        Button swapBtn = new Button("Swap");

        sellBtn.setOnAction(e->
        {
            ret.set(1);
            window.close();

        });

        toBench.setOnAction(e->
        {

            ret.set(2);
            window.close();

        });

        swapBtn.setOnAction(e->
        {
            ret.set(3);
            window.close();


        });

        VBox vBox = new VBox();
        vBox.getChildren().addAll(sellBtn, toBench, swapBtn);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);

        Scene scene = new Scene(vBox, 200, 100);
        window.setScene(scene);
        window.showAndWait();

        return ret.get();

    }
}
