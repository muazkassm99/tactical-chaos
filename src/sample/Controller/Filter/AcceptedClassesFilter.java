package sample.Controller.Filter;

import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;
import sample.Utilities.Globals;

public class AcceptedClassesFilter extends Filter {

    public AcceptedClassesFilter() {
    }

    @Override
    public boolean isAccepted(Object object) {

        Champion champion = (Champion) object;

        if(Globals.getGameChampionClasses().size() == 0)
            return true;

        for (ChampionClass chClass: Globals.getGameChampionClasses()) {
            for (ChampionClass cl : champion.getClasses()) {
                if (cl.toString().equalsIgnoreCase(chClass.toString()))
                    return true;
            }
        }
        return false;
    }
}
