package sample.Controller.Exceptions;

public class InTerrainException extends Exception{
    public InTerrainException() {

    }

    public InTerrainException(String message) {
        super(message);
    }
}
