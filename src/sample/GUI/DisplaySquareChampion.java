package sample.GUI;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;
import sample.GUI.Dialogs.TakeAction;
import sample.Model.Champion;
import sample.View.Player;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class DisplaySquareChampion {
    public static Pair<Champion, Integer> showChampions(List<Champion> champions, Player player, int ii, int jj)
    {
        Stage window = new Stage();
        AtomicReference<Pair<Champion, Integer>> championsRet = new AtomicReference<>();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("show");

        Button[] buttons = new Button[champions.size()];

        for (int i = 0; i < buttons.length; i++) {
            Champion current = champions.get(i);

            int finalI = i;
            if(current.getPlayerId() == player.getId()) {
                buttons[i] = new Button(champions.get(finalI).getName() + " (mine)");
            }
            else {
                buttons[i] = new Button(champions.get(finalI).getName());
            }

            if(current.getPlayerId()== player.getId()) {
                buttons[i].setOnAction(e -> {

                    int action = TakeAction.takeActionArena();

                    championsRet.set(new Pair(current, action));
                    player.getBattleArenaManager().removeChampionByCoordinates(current.getUniqueID(), ii, jj);

                });
            }
        }


        VBox vBox = new VBox();
        vBox.getChildren().addAll(buttons);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);

        Scene scene = new Scene(vBox, 200, 100);
        window.setScene(scene);
        window.showAndWait();

        return championsRet.get();

    }
}
