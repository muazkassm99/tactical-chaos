package sample;

import com.google.gson.Gson;
import javafx.util.Pair;
import sample.Controller.*;
import sample.Controller.Exceptions.InTerrainException;
import sample.Controller.Exceptions.OutOfArenaException;
import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;
import sample.Model.Itmes.Item;
import sample.Model.PrimitiveModels.BattleArena;
import sample.Model.PrimitiveModels.Bench;
import sample.View.BotPlayer;
import sample.View.ConsolePlayer;
import sample.View.Player;

import javax.swing.*;
import java.io.Console;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        /*BotPlayer botPlayer = new BotPlayer(1);
        botPlayer.setCoins(19);
        for (int i = 0; i < 9; i++) {

            botPlayer.startPlanningPhase();
        }

        System.out.println("Arena : ");
        for (Pair<Champion, Pair<Integer, Integer>> c: botPlayer.getBattleArenaManager().playerChampionsCoords(1)
             ) {
            System.out.println(c.getKey() + " " + c.getValue().getKey() + " " + c.getValue().getValue());
        }
        System.out.println("Bench : ");
        for (Champion c :
                botPlayer.getBenchManager().getBench().getChampions()) {
            System.out.println(c.getName());

        }
        System.out.println(botPlayer.getCoins());*/


        //cast();
        Game game = new Game();
        game.startGame();

//        BattleArenaManager battleArenaManager = new BattleArenaManager();
//        System.out.println(battleArenaManager.randomItemSpawnSelect(1));

    }

    public static void cast() {

        StoreManager storeManager = new StoreManager();

        storeManager.refresh();
        Champion champion =  storeManager.getCheapestChampionInVisibleStore();
        storeManager.sellMe(champion.getUniqueID());


        storeManager.refresh();
        Champion champion1 =  storeManager.getCheapestChampionInVisibleStore();
        storeManager.sellMe(champion.getUniqueID());

        storeManager.refresh();
        Champion champion2 =  storeManager.getCheapestChampionInVisibleStore();
        storeManager.sellMe(champion.getUniqueID());

        champion.setPlayerId(1);
        champion1.setPlayerId(1);
        champion2.setPlayerId(1);


        System.out.println(champion);
        System.out.println(champion1);
        System.out.println(champion2);

        int id = champion.getUniqueID();
        int id1 = champion1.getUniqueID();

        BattleArenaManager manager = new BattleArenaManager();
        try {
            manager.placeChampion(champion, 5, 6);
            manager.placeChampion(champion1, 10, 7);
            manager.placeChampion(champion2, 20, 10);
        } catch (OutOfArenaException | InTerrainException ignored) {

        }

        if(manager.isPlacementAllowedForPlayer(1, 10, 7))
        {
            System.out.println("Placement is allowed for player id 1");
        }
        else
        {
            System.out.println("placement is not allowed!");
        }
        if(manager.isPlacementAllowedForPlayer(1, 12, 7))
        {
            System.out.println("Placement is allowed for player id 1");
        }
        else
        {
            System.out.println("placement is not allowed!");
        }

        System.out.println("the removed champion : ");
        System.out.println(manager.removeChampionByCoordinates(id, 5, 6));

        System.out.println("champion found by id1");
        System.out.println(manager.findChampionById(id1).getName());

        System.out.println("coords of champion " + id1 );
        Pair<Integer, Integer> p = manager.findCoordinates(id1);
        System.out.println(p.getKey() + " " + p.getValue());


        for (Pair<Champion, Pair<Integer, Integer>> pairPair:
             manager.playerChampionsCoords(1)) {
            System.out.println(pairPair.getKey().getName() +
                    " " + pairPair.getValue().getKey() +
                    " " + pairPair.getValue().getValue());

        }



    }


}
