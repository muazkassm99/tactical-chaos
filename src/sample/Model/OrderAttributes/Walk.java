package sample.Model.OrderAttributes;

import sample.Model.Enums.Direction;

public class Walk extends Order {
    private Direction direction;
    private int movementSpeed;

    public Walk() {
        super(OrderClassNames.WALK.name());
    }

    public int getMovementSpeed() {
        return movementSpeed;
    }

    public void setMovementSpeed(int movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public Walk(int championId, int movementSpeed, Direction direction) {
        this();
        this.championId = championId;
        this.movementSpeed = movementSpeed;
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public int getChampionId() {
        return championId;
    }

    public void setChampionId(int championId) {
        this.championId = championId;
    }


}
