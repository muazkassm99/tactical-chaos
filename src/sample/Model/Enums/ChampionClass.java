package sample.Model.Enums;

public enum ChampionClass {
    DEMON, DRAGON, GLACIAL, IMPERIAL, NOBLE, NINJA, PIRATE, WILD, VOID, YORDLE, ASSASSIN, BLADE_MASTER, BRAWLER, ELEMENTALIST, GUNDLINGER, KNIGHT, RANGER, SHAPE_SHIFTER, SORCERER

}
