package sample.Model.Itmes;

import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;

public class UniverseCore extends Item{


    UniverseCore() {

    }

    public UniverseCore(int id)
    {
        super(ItemsClassNames.UNIVERSECORE.name(), id);
        this.setMagicResistIncrease(20);

        this.setChampionClass(ChampionClass.ELEMENTALIST);
    }


    @Override
    public String toString() {
        return "Universe Core";
    }

}
