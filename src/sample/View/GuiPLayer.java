package sample.View;

import sample.Controller.SaveGameCallBack;
import sample.Model.OrderAttributes.Order;

import java.util.List;

public class GuiPLayer extends Player {
    public GuiPLayer(int id, SaveGameCallBack saveGameCallBack) {
        super(PlayerType.GUI.name(), id, saveGameCallBack);
    }

    GuiPLayer() {

    }

    @Override
    public void upgradeChampions() {

    }

    @Override
    public void startPlanningPhase(boolean beforeNine) {

    }

    @Override
    public void startExecutionPhase() {

    }

    @Override
    public Order collectOrder() {
        return null;
    }
}
