package sample.View;

import javafx.util.Pair;
import sample.Controller.SaveGameCallBack;
import sample.Model.Champion;
import sample.Model.Enums.Direction;
import sample.Model.OrderAttributes.AbilityOrder;
import sample.Model.OrderAttributes.Attack;
import sample.Model.OrderAttributes.Order;
import sample.Model.OrderAttributes.Walk;
import sample.Utilities.Globals;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BotPlayer extends Player {

    Random random = new Random();

    BotPlayer() {

    }

    public BotPlayer(int id, SaveGameCallBack saveGameCallBack) {
        super(PlayerType.BOT.name(), id, saveGameCallBack);
    }

    @Override
    public Order collectOrder() {
        List<Pair<Champion, Pair<Integer, Integer>>> allChampions = battleArenaManager.playerChampionsCoords(id);
        Order order = null;
        for (Pair<Champion, Pair<Integer, Integer>> champ: allChampions){
            Champion champion = champ.getKey();
            int championId = champion.getUniqueID();
            if (championHasReachedMaxNumberOfOrders(championId)) {
                continue;
            }
            int orderNumber= random.nextInt(3);
            String orderType= (orderNumber==0)? "Attack": ((orderNumber==1)?"Walk":"Ability");
            if (orderType.equalsIgnoreCase("attack")) {
                Champion attackerChampion = champion;
                List<Pair<Champion, Pair<Integer, Integer>>> allChampionsenemy = battleArenaManager.getPlayerEnemiesChampionsCoordsInVision(id);
                int choice = random.nextInt(allChampionsenemy.size());
                Champion attackedChampion = allChampionsenemy.get(choice).getKey();
                order = new Attack(attackerChampion.getUniqueID(), attackedChampion.getUniqueID());
            } else if (orderType.equalsIgnoreCase("walk")) {
                Champion walkingChampion = champ.getKey();
                int directionNumber= random.nextInt(4);
                String directionChoice = (directionNumber==0)? "up": ((directionNumber==1)?"down":((directionNumber==2)?"left":"right"));
                Direction direction = null;
                if (directionChoice.equalsIgnoreCase("up")) {
                    direction = Direction.UP;
                } else if (directionChoice.equalsIgnoreCase("down")) {
                    direction = Direction.DOWN;
                } else if (directionChoice.equalsIgnoreCase("left")) {
                    direction = Direction.LEFT;
                } else {
                    direction = Direction.RIGHT;
                }
                int movementSpeed = random.nextInt(walkingChampion.getMovementSpeed());
                order = new Walk(walkingChampion.getUniqueID(), movementSpeed, direction);
            } else if (orderType.equalsIgnoreCase("ability")) {
                //if the player chooses an ability to be performed we have to check whether this ability
                //is being applied on a target or an aoe range
                //once we know that we can start creating our ability order

                int championTargetId = -1;
                if (battleArenaManager.findChampionById(championId).getAbility().getAoe() == 0) {
                    List<Pair<Champion, Pair<Integer, Integer>>> allChampionsenemy = battleArenaManager.getPlayerEnemiesChampionsCoordsInVision(id);
                    int choice = random.nextInt(allChampionsenemy.size());
                    championTargetId = allChampionsenemy.get(choice).getKey().getUniqueID();
                }
                order = new AbilityOrder(championId, championTargetId);
            }
        }
        return order;
    }

    /**
     * picks a random number between 1 and 3 and returns it
     * as it is the number of purchases a player can make
     * in the planning phase
     * <p>
     * <p>
     * //NOTE:: WHATEVER THE NUMBER WAS IT CAN NEVER PURCHASE IF
     * HE DIDN'T HAVE THE MONEY FOR IT
     *
     * @return
     */
    public int decideNumberOfPurchases() {
        return random.nextInt(3) + 1;
    }


    /**
     * this functionality does the process of purchasing champions
     * (the cheapest champions each planning phase)
     *
     * @param numberOfPurchases the number of times a bot can purchase in
     *                          the current planning phase.
     * @return a list of champions that the Bot bought
     */

    public List<Champion> purchaseInPlanningPhase(int numberOfPurchases) {
        List<Champion> championsPurchasedInPlanningPhase = new ArrayList<>();

        this.storeManager.setVisibleChampions(this.storeManager.refreshAvailableChampions());
        while ((numberOfPurchases--) != 0) {
            Champion champion = storeManager.getCheapestChampionInVisibleStore();

            if (this.purchaseChampion(champion.getUniqueID()) != null)
                championsPurchasedInPlanningPhase.add(champion);

        }
        return championsPurchasedInPlanningPhase;
    }


    /**
     * this function gets the list of champions the BOT purchased
     * and tries to place each champion in the arena if possible
     * and if it's not it will try to place it in the bench
     * <p>
     * if both cases are not applicable then it returns the remaining
     * items in the list.
     * or it returns null indicating that the list is empty and all
     * champions are placed in their places.
     *
     * @param champions
     * @return
     */
    public List<Champion> placeChampions(List<Champion> champions) {

        for (Champion ch : champions) {
            if (placeChampionInArena(ch,
                    random.nextInt(Globals.getArenaHeight()),
                    random.nextInt(Globals.getArenaWidth())) == false) {
                if (placeChampionInBench(ch) == false) {
                    return champions;
                }
            }
        }
        return null;


    }

    @Override
    public void startPlanningPhase(boolean beforeNine) {
        List<Champion> championsPurchased = purchaseInPlanningPhase(decideNumberOfPurchases());
        championsPurchased = placeChampions(championsPurchased);
        boolean sell= random.nextBoolean();
        if (championsPurchased != null && sell) {
            int rand = random.nextInt(championsPurchased.size());
            int value = championsPurchased.get(rand).getPrice();
            championsPurchased.remove(championsPurchased.get(rand));
        }
        if(!beforeNine) {
            for(int i = 0; i < random.nextInt(battleArenaManager.getBattleArenaChampions(id).size()); i++) {
                orders.add(collectOrder());
            }
        }
    }

    @Override
    public void startExecutionPhase() {
        battleArenaManager.startExecutionPhase(orders);
    }

    @Override
    public void upgradeChampions() {
        List<Champion> upgradableChampions = getListOfUpgradableChampions();
        for(Champion champion : upgradableChampions) {
            benchManager.removeChampionFromBench(champion.getUniqueID());
            Pair<Integer, Integer> coords = battleArenaManager.findCoordinates(champion.getUniqueID());
            battleArenaManager.removeChampionByCoordinates(champion.getUniqueID(), coords.getKey(), coords.getValue());
            levelUpChampion(champion);
            placeChampionInArena(champion, random.nextInt(Globals.getArenaWidth()), random.nextInt(Globals.getArenaHeight()));
        }
    }
}











