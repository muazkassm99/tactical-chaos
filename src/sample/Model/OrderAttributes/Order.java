package sample.Model.OrderAttributes;

public abstract class Order {
    protected int championId;
    String orderType;

    Order() {

    }

    public Order(String orderType) {
        this.orderType = orderType;
    }

    public int getChampionId() {
        return championId;
    }

    public void setChampionId(int championId) {
        this.championId = championId;
    }
}
