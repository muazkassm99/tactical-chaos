package sample.Model.Itmes;

import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;

public class MagicHat extends Item {

    MagicHat() {

    }

    public MagicHat(int id)
    {
        super(ItemsClassNames.MAGICHAT.name(), id);
        this.setAbilityDamageIncrease(20);
        this.setChampionClass(ChampionClass.SORCERER);

    }


    @Override
    public String toString() {
        return "Magic Hat";
    }

}
