package sample.GUI;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;
import sample.Utilities.Globals;
import sample.View.ConsolePlayer;
import sample.View.GuiPLayer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeScreen {
    private static Stage primaryStage;
    private static Map<String, Object> settingsMap = new HashMap<>();
    private static Button playNowBtn ;
    private static Button settingsBtn;
    private static VBox layout;
    private static StartGame game;


    public static void display()
    {


        primaryStage = new Stage();


        initButtons();

        initLayout();

        Scene startScene = new Scene(layout, 600, 400);

        primaryStage.setScene(startScene);
        primaryStage.show();
    }

    private static void initLayout() {
        layout = new VBox(30);
        layout.getChildren().addAll(playNowBtn, settingsBtn);
        layout.setAlignment(Pos.CENTER);
    }

    private static void initButtons() {
        playNowBtn = new Button("Play Now");
        settingsBtn = new Button("Settings");


        //scaling buttons
        playNowBtn.setScaleX(1.5);
        playNowBtn.setScaleY(1.5);
        settingsBtn.setScaleX(1.5);
        settingsBtn.setScaleY(1.5);


        //setting buttons on action :
        settingsBtn.setOnAction(e-> {
            settingsMap.putAll( Settings.display() );
        });

        playNowBtn.setOnAction(e->
        {
            if(!settingsMap.isEmpty()) {
                /*System.out.println(settingsMap.get("height").toString());
                System.out.println(settingsMap.get("width").toString());
                System.out.println(settingsMap.get("maxSwaps").toString());
                System.out.println(settingsMap.get("maxTeam").toString());
                System.out.println(settingsMap.get("maxRounds").toString());
                for (ChampionClass c :
                        (List<ChampionClass>) settingsMap.get("acceptedClasses")) {
                    System.out.println(c);
                }*/

                //SETTINGS ON GLOBAL:
                Integer height = (Integer) settingsMap.get("height");
                Integer width = (Integer) settingsMap.get("width");
                Integer maxSwaps = (Integer) settingsMap.get("maxSwaps");
                Integer maxTeam = (Integer)settingsMap.get("maxTeam");
                Integer maxRounds = (Integer)settingsMap.get("maxRounds");
                List<ChampionClass> championClasses = (List<ChampionClass>)settingsMap.get("acceptedClasses");
                Globals.setArenaHeight(height);
                Globals.setArenaWidth(width);
                Globals.setNumberOfChampionsOnArena(maxTeam);
                Globals.setNumberOfRounds(maxRounds);
                Globals.setMaxNumberOfSwaps(maxSwaps);
                Globals.setGameChampionClasses(championClasses);

            }

            // todo GuiPLayer  guiPLayer = new GuiPLayer(1);
//            guiPLayer.setCoins(10);
//            game = new StartGame(primaryStage, guiPLayer);
//            game.start();




        });

    }

    public static StartGame getGameObject()
    {
        return game;
    }


}
