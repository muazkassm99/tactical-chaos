package sample.Model.PrimitiveModels;

import sample.Model.Champion;

import java.util.ArrayList;
import java.util.List;

public class Bench {
    private List<Champion> champions;

    public Bench() {
        champions = new ArrayList<>();
    }

    public List<Champion> getChampions() {
        return champions;
    }

    public void setChampions(List<Champion> champions) {
        this.champions = champions;
    }
}
