package sample.Controller;

import sample.Controller.Exceptions.InTerrainException;
import sample.Controller.Exceptions.OutOfArenaException;
import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;
import sample.Model.Enums.TileType;
import sample.Model.Itmes.Item;
import sample.Model.PrimitiveModels.BattleArena;
import sample.Utilities.Globals;
import sample.View.BotPlayer;
import sample.View.ConsolePlayer;
import sample.View.GuiPLayer;
import sample.View.Player;

import java.util.*;

public class MasterManager {
    private List<Player> players;
    private transient SaveGameCallBack saveGameCallBack;
    private int numberOfRounds;
    private int planningRounds;
    private int playerIndex;

    private int numberOfPlayers;
    private int numberOfVisibleChampions;
    private int numberOfChampionsOnBench;
    private int numberOfChampionsOnArena;
    private int maxNumberOfPurchasesInRound;
    private int arenaHeight;
    private int arenaWidth;
    private int maxTeamSize;
    private int maxNumberOfSwaps;
    private int maxNumberOfRounds;
    private List<ChampionClass> gameChampionClasses;

    private BuffManager buffManager;

    public MasterManager()
    {
        //DETECT PLAYERS TYPE
        this.saveGameCallBack = saveGameCallBack;
        this.players = initPlayers();
        numberOfRounds = Globals.getNumberOfRounds();
        planningRounds = 0;
        playerIndex = 0;
        buffManager = new BuffManager();
    }

    public MasterManager(SaveGameCallBack saveGameCallBack) {

        //DETECT PLAYERS TYPE
        this.saveGameCallBack = saveGameCallBack;
        this.players = initPlayers();
        numberOfRounds = Globals.getNumberOfRounds();
        planningRounds = 0;
        playerIndex = 0;
        buffManager = new BuffManager();
    }


    public void executeRounds() {

        while(numberOfRounds-- != 0) {

            /*
             * do pre-9-rounds planning phase for all the players
             * then start doing the normal planning and execution
             */

            //if planning rounds has ended
            if(planningRounds == 9)
            {
                for (int i = playerIndex; i < players.size(); i++) {
                    Player player = players.get(i);
                    player.setCoins(16);
                }
                mergePlayerBattleArenas();
                planningRounds++;
            }

            if(planningRounds < 9) {
                for (int i = playerIndex; i < players.size(); i++) {
                    Player player = players.get(i);
                    player.setCoins(1000);
                    player.getStoreManager().refresh();
                    player.startPlanningPhase(true);
                }
                planningRounds++;
            } else {
                for (int i = playerIndex; i < players.size(); i++) {
                    Player player = players.get(i);
                    player.setCoins(player.getCoins() + 2);
                    player.startPlanningPhase(false);
                }

                Collections.shuffle(players);
                for (int i = playerIndex; i < players.size(); i++) {
                    Player player = players.get(i);
                    player.startExecutionPhase();
                    player.removeDeadChampions();
                    player.getOrders().clear();
                }
            }

            Globals.setCurrentRound(Globals.getCurrentRound() + 1);
        }
    }


    /**
     * First Method to use in the entire code
     * used to get the number of players and
     * their types from the user from a starting
     * console screen
     * ...
     */
    public List<Player> initPlayers() {

        List<Player> players = new ArrayList<>();

        //setting the number of players in the game ..
        System.out.print("Enter the number of players (from 2 to 4): ");
        Globals.setNumberOfPlayers(Globals.getScanner().nextInt());

        //setting the players types ..
        System.out.println("'gui, console or bot .. ");
        for (int i = 0; i < Globals.getNumberOfPlayers(); i++) {
            System.out.print("Player number " + (i + 1) + " type : ");
            String type = Globals.getScanner().next();
            switch (type) {
                case "bot":
                    players.add(new BotPlayer(i + 1, saveGameCallBack));
                    break;

                case "console":
                    players.add(new ConsolePlayer(i + 1, saveGameCallBack));
                    break;

                case "gui":
                    players.add(new GuiPLayer(i + 1, saveGameCallBack));
                    break;
            }
        }

        return players;
    }

    void mergePlayerBattleArenas() {

        //setting all champions on the same arena.
        BattleArena battleArena = new BattleArena(Globals.getArenaHeight(), Globals.getArenaHeight());
        for(Player player : players) {
            for(int i = 0; i < Globals.getArenaWidth(); i++) {
                for(int j = 0; j < Globals.getArenaHeight(); j++) {
                    for(Champion champion : player.getBattleArenaManager().getArena().getSquares()[i][j].getChampions()) {
                        battleArena.getSquares()[i][j].getChampions().add(champion);
                    }
                }
            }
        }


        // **************** when here now we are working on one arena for all the players.

        //creating the list of tile types
        List<TileType> types = generateListOfTileTypes();

        //picking the tile type and assigning the type of the square in arena to the picked type
        for(int i = 0; i < Globals.getArenaWidth(); i++) {
            for (int j = 0; j < Globals.getArenaHeight(); j++) {
                Collections.shuffle(types);
                TileType tileType = types.get(0);
                battleArena.getSquares()[i][j].setType(tileType);
            }
        }

        //loops through all champions and place them out of a terrain tile
        for(int i = 0; i < Globals.getArenaWidth(); i++) {
            for (int j = 0; j < Globals.getArenaHeight(); j++) {
                if(battleArena.getSquares()[i][j].getType() == TileType.TERRAIN) {
                    for(Champion champion : battleArena.getSquares()[i][j].getChampions()) {
                        placeChampionOutOfTerrain(champion, i, j);
                    }
                }
            }
        }



        for(Player player : players) {
            player.getBattleArenaManager().setArena(battleArena);
        }
        //todo  :: add the items to the random-selected squares.
        List<Item> items = generateListOfItems();

        //each item will be randomly put in a square ..

        for (Item it : items) {
            this.players.get(0).getBattleArenaManager().spawnItem(it);
        }

        /**
         code goes here.
         */
    }

    public List<Item> generateListOfItems() {

        List<Item> items = new ArrayList<>();

        for (int i = 0; i < Globals.getMaxItemSize(); i++) {
            Item item =players.get(0).getBattleArenaManager().randomItemSpawnSelect(i+1);
            items.add(item);
        }

        return items;
    }

    /**
     * creates a list of tile types enums, some items are duplicated so it can have more pick rate
     * for example the standard type is duplicated three more times so it can have a pick rate of 50%
     * @return the actual list
     */
    private List<TileType> generateListOfTileTypes() {
        List<TileType> types = new ArrayList<>();
        types.add(TileType.STANDARD);
        types.add(TileType.STANDARD);
        types.add(TileType.STANDARD);
        types.add(TileType.WATER);
        types.add(TileType.WATER);
        types.add(TileType.GRASS);
        types.add(TileType.GRASS);
        types.add(TileType.TERRAIN);
        return types;
    }

    /**
     * this function is used when a champion placed in a terrain tile to place it in a contiguous tile
     * @param champion the champion in terrain tile
     * @param posI the initial position i
     * @param posJ the initial position j
     */
    private void placeChampionOutOfTerrain(Champion champion, int posI, int posJ) {
        while(true) {
            Random random = new Random();
            int randomTileNumber = random.nextInt(9) + 1;
            switch(randomTileNumber) {
                case 1:
                    posI--;
                    posJ--;
                case 2:
                    posJ--;
                case 3:
                    posI++;
                    posJ--;
                case 4:
                    posI--;
                case 5:
                    continue;
                case 6:
                    posI++;
                case 7:
                    posI--;
                    posJ++;
                case 8:
                    posJ++;
                case 9:
                    posI++;
                    posJ++;
            }
            try {
                //stupid solution but works
                players.get(0).getBattleArenaManager().placeChampion(champion, posI, posJ);
                if(players.get(0).getBattleArenaManager().getArena().getSquares()[posI][posJ].getType() == TileType.TERRAIN) {
                    placeChampionOutOfTerrain(champion, posI, posJ);
                }
            } catch(IndexOutOfBoundsException e) {
                continue;
            } catch(OutOfArenaException | InTerrainException ignored) {

            }
            break;
        }
    }


    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void saveGlobalVariables() {
        numberOfPlayers = Globals.getNumberOfPlayers();
        numberOfVisibleChampions = Globals.getNumberOfVisibleChampions();
        numberOfChampionsOnBench = Globals.getNumberOfChampionsOnBench();
        numberOfChampionsOnArena = Globals.getNumberOfChampionsOnArena();
        maxNumberOfPurchasesInRound = Globals.getMaxNumberOfPurchasesInRound();
        arenaHeight = Globals.getArenaHeight();
        arenaWidth = Globals.getArenaWidth();
        maxTeamSize = Globals.getMaxTeamSize();
        maxNumberOfSwaps = Globals.getMaxNumberOfSwaps();
        maxNumberOfRounds = Globals.getNumberOfRounds();
        gameChampionClasses = Globals.getGameChampionClasses();
    }

    public void loadGameVariables() {
        Globals.setNumberOfPlayers(numberOfPlayers);
        Globals.setNumberOfVisibleChampions(numberOfVisibleChampions);
        Globals.setNumberOfChampionsOnBench(numberOfChampionsOnBench);
        Globals.setNumberOfChampionsOnArena(numberOfChampionsOnArena);
        Globals.setMaxNumberOfPurchasesInRound(maxNumberOfPurchasesInRound);
        Globals.setArenaHeight(arenaHeight);
        Globals.setArenaWidth(arenaWidth);
        Globals.setMaxTeamSize(maxTeamSize);
        Globals.setMaxNumberOfSwaps(maxNumberOfSwaps);
        Globals.setNumberOfRounds(maxNumberOfRounds);
        Globals.setGameChampionClasses(gameChampionClasses);
    }

    public void setSaveGameCallBack(SaveGameCallBack saveGameCallBack) {
        this.saveGameCallBack = saveGameCallBack;
        for(Player player : players){
            player.setSaveGameCallBack(saveGameCallBack);
        }
    }
}
