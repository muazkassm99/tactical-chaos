package sample.Utilities;

import sample.Controller.BattleArenaManager;
import sample.Model.Ability;
import sample.Model.OrderAttributes.AbilityOrder;
import sample.Model.OrderAttributes.Attack;
import sample.Model.OrderAttributes.Order;
import sample.Model.OrderAttributes.Walk;
import sample.Model.PrimitiveModels.BattleArena;

import javax.swing.*;

public class OrderThread extends Thread {
    BattleArenaManager.OrderManager orderManager;
    Order order;

    OrderThread() {

    }

    public OrderThread(BattleArenaManager.OrderManager orderManager, Order order) {
        this.orderManager = orderManager;
        this.order = order;
    }

    @Override
    public void run() {
        if(order instanceof Walk) {
            orderManager.moveMe((Walk)order);
        } else if(order instanceof Attack) {
            orderManager.performAttack((Attack) order);
        } else if(order instanceof AbilityOrder) {
            orderManager.performAbility((AbilityOrder) order);
        }
    }
}
