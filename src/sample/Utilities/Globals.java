package sample.Utilities;

import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;

import java.util.*;

public class Globals {
    private static Scanner scanner = new Scanner(System.in);
    private static int numberOfPlayers;
    private static int numberOfVisibleChampions = 5;
    private static int numberOfChampionsOnBench = 8;
    private static int numberOfChampionsOnArena = 9;
    private static int maxNumberOfPurchasesInRound = 3;
    private static int arenaHeight = 20;
    private static int arenaWidth = 20;
    private static int maxTeamSize = 9;
    private static int numberOfRounds = 18;
    private static int maxNumberOfSwaps = 3;
    private static List<ChampionClass> gameChampionClasses = new ArrayList<>();
    private static boolean recordGame = false;
    private static int maxItemSize = 20;
    private static int currentRound = 1;
    private static int maxItemsOnChampion = 3;

    public static Scanner getScanner() {
        return scanner;
    }

    public static int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public static void setNumberOfPlayers(int numberOfPlayers) {
        Globals.numberOfPlayers = numberOfPlayers;
    }

    public static int getNumberOfVisibleChampions() {
        return numberOfVisibleChampions;
    }

    public static void setNumberOfVisibleChampions(int numberOfVisibleChampions) {
        Globals.numberOfVisibleChampions = numberOfVisibleChampions;
    }

    public static int getNumberOfChampionsOnBench() {
        return numberOfChampionsOnBench;
    }

    public static void setNumberOfChampionsOnBench(int numberOfChampionsOnBench) {
        Globals.numberOfChampionsOnBench = numberOfChampionsOnBench;
    }

    public static int getNumberOfChampionsOnArena() {
        return numberOfChampionsOnArena;
    }

    public static void setNumberOfChampionsOnArena(int numberOfChampionsOnArena) {
        Globals.numberOfChampionsOnArena = numberOfChampionsOnArena;
    }

    public static int getMaxNumberOfPurchasesInRound() {
        return maxNumberOfPurchasesInRound;
    }

    public static void setMaxNumberOfPurchasesInRound(int maxNumberOfPurchasesInRound) {
        Globals.maxNumberOfPurchasesInRound = maxNumberOfPurchasesInRound;
    }

    public static int getArenaHeight() {
        return arenaHeight;
    }

    public static void setArenaHeight(int arenaHeight) {
        Globals.arenaHeight = arenaHeight;
    }

    public static int getArenaWidth() {
        return arenaWidth;
    }

    public static void setArenaWidth(int arenaWidth) {
        Globals.arenaWidth = arenaWidth;
    }

    public static int getMaxTeamSize() {
        return maxTeamSize;
    }

    public static void setMaxTeamSize(int maxTeamSize) {
        Globals.maxTeamSize = maxTeamSize;
    }

    public static int getNumberOfRounds() {
        return numberOfRounds;
    }

    public static void setNumberOfRounds(int numberOfRounds) {
        Globals.numberOfRounds = numberOfRounds;
    }

    public static int getMaxNumberOfSwaps() {
        return maxNumberOfSwaps;
    }

    public static void setMaxNumberOfSwaps(int maxNumberOfSwaps) {
        Globals.maxNumberOfSwaps = maxNumberOfSwaps;
    }

    public static List<ChampionClass> getGameChampionClasses() {
        return gameChampionClasses;
    }

    public static void setGameChampionClasses(List<ChampionClass> gameChampionClasses) {
        Globals.gameChampionClasses = gameChampionClasses;
    }

    public static boolean isGameRecording() {
        return recordGame;
    }

    public static void setRecordGame(boolean recordGame) {
        Globals.recordGame = recordGame;
    }

    public static int getMaxItemSize() {
        return maxItemSize;
    }

    public static void setMaxItemSize(int maxItemSize) {
        Globals.maxItemSize = maxItemSize;
    }

    public static int getCurrentRound() {
        return currentRound;
    }

    public static void setCurrentRound(int currentRound) {
        Globals.currentRound = currentRound;
    }

    public static int getMaxItemsOnChampion() {
        return maxItemsOnChampion;
    }

    public static void setMaxItemsOnChampion(int maxItemsOnChampion) {
        Globals.maxItemsOnChampion = maxItemsOnChampion;
    }
}
