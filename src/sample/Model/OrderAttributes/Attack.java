package sample.Model.OrderAttributes;

public class Attack extends Order {
    private int championDestinationId;

    Attack() {
        super(OrderClassNames.ATTACK.name());
    }

    public Attack(int championId, int championDestinationId) {
        this();
        this.championId = championId;
        this.championDestinationId = championDestinationId;
    }

    public int getChampionDestinationId() {
        return championDestinationId;
    }

    public void setChampionDestinationId(int championDestinationId) {
        this.championDestinationId = championDestinationId;
    }
}