package sample.Model.Itmes;

import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;
import sample.View.Player;

public abstract class Item {

    private int uniqueID;

    private int abilityDamageIncrease;
    private int physicalDamageIncrease;
    private int armorIncrease;
    private int criticalStrikeIncrease;
    private int maxHealthIncrease;
    private int magicResistIncrease;

    private ChampionClass championClass;

    private String itemType;

    Item() {

    }

    protected Item(String itemType, int uniqueID)
    {
        this.itemType = itemType;
        abilityDamageIncrease = 0;
        physicalDamageIncrease = 0;
        armorIncrease = 0;
        criticalStrikeIncrease = 0;
        maxHealthIncrease = 0;
        magicResistIncrease = 0;

        this.uniqueID = uniqueID;
//        abilityDamageIncrease.setValue(0);
//        physicalDamageIncrease.setValue(0);
//        armorIncrease.setValue(0);
//        criticalStrikeIncrease.setValue(0);
//        maxHealthIncrease.setValue(0);
//        magicResistIncrease.setValue(0);
    }


    public int getAbilityDamageIncrease() {
        return abilityDamageIncrease;
    }

    public void setAbilityDamageIncrease(int abilityDamageIncrease) {
        this.abilityDamageIncrease = abilityDamageIncrease;
    }

    public int getPhysicalDamageIncrease() {
        return physicalDamageIncrease;
    }

    public void setPhysicalDamageIncrease(int physicalDamageIncrease) {
        this.physicalDamageIncrease = physicalDamageIncrease;
    }

    public int getArmorIncrease() {
        return armorIncrease;
    }

    public void setArmorIncrease(int armorIncrease) {
        this.armorIncrease = armorIncrease;
    }

    public int getCriticalStrikeIncrease() {
        return criticalStrikeIncrease;
    }

    public void setCriticalStrikeIncrease(int criticalStrikeIncrease) {
        this.criticalStrikeIncrease = criticalStrikeIncrease;
    }

    public int getMaxHealthIncrease() {
        return maxHealthIncrease;
    }

    public void setMaxHealthIncrease(int maxHealthIncrease) {
        this.maxHealthIncrease = maxHealthIncrease;
    }

    public int getMagicResistIncrease() {
        return magicResistIncrease;
    }

    public void setMagicResistIncrease(int magicResistIncrease) {
        this.magicResistIncrease = magicResistIncrease;
    }

    public ChampionClass getChampionClass() {
        return championClass;
    }

    public void setChampionClass(ChampionClass championClass) {
        this.championClass = championClass;
    }


    public int getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(int uniqueID) {
        this.uniqueID = uniqueID;
    }

    @Override
    public String toString() {
        return "apDmgInc  : " + abilityDamageIncrease + " - physicalDmgInc " + physicalDamageIncrease
                + " - armorInc : " + armorIncrease+ " - criticalChanceInc : " + criticalStrikeIncrease
                + " - maxHealthInc : " + maxHealthIncrease + " - magicResistInc : " + magicResistIncrease;
    }
}










































































