package sample.Model.Itmes;

import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;

public class WarirorGlove extends Item {

    WarirorGlove() {

    }

    public WarirorGlove(int id)
    {
        super(ItemsClassNames.WARIORGLOVE.name(), id);
        this.setPhysicalDamageIncrease(10);
        this.setChampionClass(ChampionClass.BLADE_MASTER);
    }


    @Override
    public String toString() {
        return "Warrior Glove";
    }

}
