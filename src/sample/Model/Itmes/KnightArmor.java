package sample.Model.Itmes;

import sample.Model.Champion;
import sample.Model.Enums.ChampionClass;

public class KnightArmor extends Item {

    KnightArmor() {

    }

    public KnightArmor(int id)
    {
        super(ItemsClassNames.KNIGHTARMOR.name(), id);
        this.setArmorIncrease(15);

        this.setChampionClass(ChampionClass.KNIGHT);
    }


    @Override
    public String toString() {
        return "Knight Armor";
    }
}
