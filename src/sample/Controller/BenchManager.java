package sample.Controller;

import sample.Model.Enums.ChampionPlace;
import sample.Model.PrimitiveModels.Bench;
import sample.Model.Champion;
import sample.Utilities.Globals;

import java.util.List;

public class BenchManager {
    private Bench bench;

    public BenchManager() {
        bench = new Bench();
    }


    /**
     * adds the champion to the bench
     * @param champion the champion we want to add
     * @return true if added and false otherwise
     */
    public Boolean addChampionToBench(Champion champion) {

        //CHECKS FOR MAXIMUM SIZE
        if(bench.getChampions().size() < Globals.getNumberOfChampionsOnBench())
        {
            champion.setPlace(ChampionPlace.BENCH);
            bench.getChampions().add(champion);
            return true;
        }
        return false;
    }


    //****************************************************************************

    /**
     * removes a champion from the bench depending on their id.
     *
     *
     * @param championId the unique id of champ we want to delete from bench.
     * @return the Champion deleted from the bench.
     */
    public Champion removeChampionFromBench(int championId) {
        for (Champion champ : bench.getChampions()) {
            if (champ.getUniqueID() == championId) {
                Champion returnedChampion = champ;
                bench.getChampions().remove(champ);
                return returnedChampion;
            }
        }
        return null;
    }


    //****************************************************************************

    /**
     * for displaying the bench ..
     */

    public void displayBench()
    {
        for (Champion c: this.getBench().getChampions()) {
            System.out.println(c.getName());
        }
    }



    //****************************************************************************

    public List<Champion> getBenchChampions() {
        return bench.getChampions();
    }

    //****************************************************************************


    /**
     *
     *
     *
     *
     * setters and getters ....
     *
     *
     *
     *
     *
     */

    public Bench getBench() {
        return bench;
    }

    public void setBench(Bench bench) {
        this.bench = bench;
    }
}
