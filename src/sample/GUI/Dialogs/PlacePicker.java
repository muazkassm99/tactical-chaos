package sample.GUI.Dialogs;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.concurrent.atomic.AtomicBoolean;

public class PlacePicker {
    public static boolean putInBench()
    {
        Stage window = new Stage();
        AtomicBoolean value = new AtomicBoolean(true);

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Chose a place : ");

        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.getItems().add("bench");
        comboBox.getItems().add("arena");
        comboBox.setValue("bench");


        Button closeBtn = new Button("select");



        window.setOnCloseRequest(e->
                window.close()
        );

        closeBtn.setOnAction(e->{

            String valueS = comboBox.getValue();
            if(valueS.equals("arena"))
                value.set(false);


            window.close();

        });

        VBox vBox = new VBox();
        HBox hBox = new HBox();
        vBox.getChildren().addAll(comboBox, hBox, closeBtn);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);

        Scene scene = new Scene(vBox, 200, 100);
        window.setScene(scene);
        window.showAndWait();

        return value.get();

    }
}
